<?php
include 'modules/smarty/Smarty/libs/Smarty.class.php';
include 'autoload.php';
$login = new Login;
$smarty = new Smarty;
$smarty->setTemplateDir('modules/smarty/templates')
        ->setCompileDir('modules/smarty/templates_c');

$username = 0;
$level = 5;


Login::startSession();
//var_dump($_SESSION);
if (Login::sessionIsSet()){

    $login->showControlPanel();
    $utility = new Utility;
    $user = new User;
    $user_level = $user->getLevelByUser($_SESSION['login_user']);
    $permissions = $utility->getPermissions();
//    var_dump($permissions);
    Utility::showMenubar();
    
    //var_dump($user_level);

    if ($utility->existsMenu($_GET['menu'])){
        
        $key = $utility->getPermissionKey($_GET['menu']);
        
        if($permissions[$key] === '1'){
            
            $classname = $utility->getClass($_GET['menu']);
            $function = $utility->getFunction($_GET['menu']);
            $class = new $classname;
            $class->$function();
            
        } else {
            echo 'Nincs jogosultsagod hozzaferni a tartalomhoz';
        }
        
    }
    
} else {
    $login->showLoginForm();
  }