<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&action=submit" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update" method="post" class="basic-grey">
    {/if}
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>
          <span>Name :</span>
          <input id="name" required type="text" name="name" placeholder="Position's name" value="{$name}" />
      </label>
      
      <!--<label>
          <span>Rank :</span>
          <select required name="rank" id="rank">
            {if $rank eq ''}
            <option selected="">Select...</option>
            {elseif $rank gt "0"}
            <option selected="{$rank}">{$rank}</option>
            {/if}
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
      </label>-->
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=position">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>