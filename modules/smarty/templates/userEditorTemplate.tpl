<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&action=submit" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update" method="post" class="basic-grey">
    {/if}
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      {if $id eq ''}
      <label>   
          <span>Employee :</span>
          <select name="employee_name" id="employee_name">
            {if $employee_name eq ''}
            <option selected value="">(Optional)</option>
            {else}
            <option selected value="{$employee_name}">{$employee_name}</option>
            {/if}
            {foreach from=$employees item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      {/if}
      
      <label>
          <span>Username :</span>
          <input id="username" required type="text" name="username" value="{$username}" />
      </label>
      
      <label>
          <span>Password :</span>
          <input id="password" required type="text" name="password" value="{$password}" />
      </label>
      
      <label>
          <span>Level :</span>
          <input id="level" required type="number" name="level" value="{$level}" />
      </label>
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=user">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>