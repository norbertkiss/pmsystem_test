<html>
    <head>
        <title>Login Form in PHP with Session</title>
        <link rel="stylesheet" media="screen" type="text/css" href="modules/css/login.css">
    </head>
    
    <body align="center">
            
        <div id="main" align="center">
            
            <h1>ERP System</h1>
            
            <div id="login">
                
                <h2>Login</h2>
            
                <form action="" method="post">
                    
                    <label>Username :</label>
                    <input id="name" name="username" placeholder="username" type="text">
                    
                    <label>Password :</label>
                    <input id="password" name="password" placeholder="**********" type="password">
                    
                    <input name="submit" type="submit" value=" Login ">
                    <span>{$error}</span>
                    
                </form>
            </div>
        </div>
    </body>
</html>