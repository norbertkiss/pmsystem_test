<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {if $permissions['add_tasks'] eq '1'}<button onclick="self.location.href=('{$editor}{if $project_id neq ''}&project_id={$project_id}{/if}')">New {$title}</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}&nbsp;</th>
            {/foreach}
            {if $permissions['show_task_start'] eq '1'}<th></th>{/if}
            {if $permissions['show_task_stop'] eq '1'}<th class="th"></th>{/if}
            <th class="th"></th>
            {if $permissions['show_task_employees'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['edit_tasks'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['delete_tasks'] eq '1'}<th class="th"></th>{/if}
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
          <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{$v['pname']}</td>
            <td class="left_align" title="{$v['tname']}">{$v['tname']|truncate:10}</td>
            <td class="left_align">{$v['priority']}</td>
            <td class="left_align">{$v['status']}</td>
            <td class="left_align" title="{$v['description']}">{$v['description']|truncate:15}</td>
            <td class="left_align" title="{$v['start_date']}">{if $v['start_date'] eq '0000-00-00 00:00:00'}-{else}{$v['start_date']|truncate:13}{/if}</td>
            <td class="left_align" title="{$v['complete_date']}">{if $v['complete_date'] eq '0000-00-00 00:00:00'}-{else}{$v['complete_date']|truncate:13}{/if}</td>
            <td class="left_align">{if $v['deadline'] eq '0000-00-00'}-{else}{$v['deadline']}{/if}</td>
            {if $permissions['show_task_start'] eq '1'}<td class="centered"><a href="?menu={$title}&id={$v['id']}&action=start"><button>Start</button></a></td>{/if}
            {if $permissions['show_task_stop'] eq '1'}<td class="centered"><a href="?menu={$title}&id={$v['id']}&action=reset"><button>Reset</button></a></td>{/if}
            <td class="centered"><a href="?menu={$title}&id={$v['id']}&action=finish"><button>Finish</button></a></td>
            {if $permissions['show_task_employees'] eq '1'}<td class="centered"><a href="?menu=employees_on_task&id={$v['id']}{if $project_id neq ''}&project_id={$project_id}{/if}"><button>Employees ({$v['employee_count']})</button></a></td>{/if}
            {if $permissions['edit_tasks'] eq '1'}<td class="centered"><a href="?menu={$title}_editor&id={$v['id']}{if $project_id neq ''}&project_id={$project_id}{/if}"><button class="edit"></button></a></td>{/if}
            {if $permissions['delete_tasks'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu={$title}&id={$v['id']}&action=delete{if $project_id neq ''}&project_id={$project_id}{/if}"><button class="delete"></button></a></td>{/if}
          </tr>
        {/foreach}
    
    </table>
  </body>
</html>