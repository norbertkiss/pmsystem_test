<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&action=submit" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update" method="post" class="basic-grey">
    {/if}
      <h1>Employee Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>
          <span>Name :</span>
          <input id="name" required type="text" name="name" placeholder="Employee's Full Name" value="{$name}" />
      </label>
      
      {if $id eq ''}
      <label>   
          <span>Position :</span>
          <select required name="position_name" id="position_name">
            {if $position_name eq ''}
            <option selected="">Select...</option>
            {else}
            <option selected="{$position_name}">{$position_name}</option>
            {/if}
            {foreach from=$positions item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      {/if}
      
      <label>
          <span>Email :</span>
          <input id="email" required type="email" name="email" placeholder="Valid Email Address" value="{$email}" />
      </label>
      
      <label>
          <span>Phone :</span>
          <input id="phone" required type="phone" name="phone" placeholder="(0036301234567)" value="{$phone}" />
      </label>
      
      <label>
          <span>Address :</span>
          <textarea id="address" required name="address" placeholder="Valid Address" >{$address}</textarea>
      </label>
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=employee">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>