<html>
  <head>
    <title>Add role</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    
    <form action="?menu=user_roles_editor&id={$id}&action=submit" method="post" class="basic-grey">
    
    
   
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>   
          <span>Role :</span>
          <select required name="role" id="role">
            {if $role eq ''}
            <option selected="">Select...</option>
            {else}
            <option selected="{$role}">{$role}</option>
            {/if}
            {foreach from=$roles item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=user_roles&id={$id}">Back</a>
            </div>
        </div>
  </body>
</html>