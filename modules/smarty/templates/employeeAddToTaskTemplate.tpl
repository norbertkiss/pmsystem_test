<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    
    <form action="?menu=add_employee_to_task&id={$id}&action=submit{if $project_id neq ''}&project_id={$project_id}{/if}" method="post" class="basic-grey">
    
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>   
          <span>Employee :</span>
          <select required name="employee" id="employee">
            
            <option selected value="">Select...</option>
            
        
            {foreach from=$employees item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      
     
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=employees_on_task&id={$id}{if $project_id neq ''}&project_id={$project_id}{/if}">Back</a>
            </div>
    </form>
  </body>
</html>