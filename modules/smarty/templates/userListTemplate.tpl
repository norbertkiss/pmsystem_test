<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {if $permissions['add_users'] eq '1'}<button onclick="self.location.href=('{$editor}')">New {$title}</button>{/if}
    
    <table class = "bordered">
        <thead>
            <tr>
                {foreach from=$headers item=value}
                    <th>{$value}</th>
                {/foreach}
                {if $permissions['show_user_roles'] eq '1'}<th></th>{/if}
                {if $permissions['edit_users'] eq '1'}<th class="th"></th>{/if}
                {if $permissions['delete_users'] eq '1'}<th class="th"></th>{/if}
            </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
          <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{if $v['name'] eq ''}N/A{else}{$v['name']}{/if}</td>
            <td class="left_align">{$v['username']}</td>
            <td class="left_align">{$v['password']}</td>
            {if $permissions['show_user_roles'] eq '1'}<td class="centered"><a href="?menu=user_roles&id={$v['id']}"><button>Roles</button></a></td>{/if}
            {if $permissions['edit_users'] eq '1'}<td class="centered"><a href="?menu={$title}_editor&id={$v['id']}"><button class="edit"></button></a></td>{/if}
            {if $permissions['delete_users'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu={$title}&id={$v['id']}&action=delete"><button class="delete"></button></a></td>{/if}
          </tr>
        {/foreach}
    
    </table>
  </body>
</html>