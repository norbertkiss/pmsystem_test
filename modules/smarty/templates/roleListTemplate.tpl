<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {if $permissions['add_roles'] eq '1'}<button onclick="self.location.href=('?menu=role_editor')">New Role</button>{/if} 
    
    <table class = "bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Role</th>
            {if $permissions['edit_roles'] eq '1'}<th></th>{/if}
            {if $permissions['delete_roles'] eq '1'}<th class="th"></th>{/if}
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{$v['name']}</td>
            {if $permissions['edit_roles'] eq '1'}<td class="centered"><a href="?menu=role_editor&id={$v['id']}"><button class="edit"></button></a></td>{/if}
            {if $permissions['delete_roles'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=role&id={$v['id']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
  </body>
</html>