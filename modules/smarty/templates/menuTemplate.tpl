
<html >
    
    <head>
    <title>Emese-soft</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="modules/css/menu.css" type="text/css" />
    </head>
    
    <body>
    <h1>ERP System</h1>
    <ul id="nav">
      {if $permissions['show_users'] eq '1'}<li><a href="?menu=user">Users</a></li>{/if}
      {if $permissions['show_roles'] eq '1'}<li><a href="?menu=role">Roles</a></li>{/if}
      {if $permissions['show_employees'] eq '1'}<li><a href="?menu=employee">Employees</a></li>{/if}
      {if $permissions['show_positions'] eq '1'}<li><a href="?menu=position">Positions</a></li>{/if}
      {if $permissions['show_projects'] eq '1'}<li><a href="?menu=project">Projects</a></li>{/if}
      {if $permissions['show_tasks'] eq '1'}<li><a href="?menu=task">Tasks</a></li>{/if}
    </ul>

