<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    
    <form action="{$SCRIPT_NAME}&id={$id}&action=submit" method="post" class="basic-grey">
    
    
   
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <label>   
          <span>Position :</span>
          <select required name="position_name" id="position_name">
            {if $position_name eq ''}
            <option selected="">Select...</option>
            {else}
            <option selected="{$position_name}">{$position_name}</option>
            {/if}
            {foreach from=$positions item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=employee_position&id={$id}">Back</a>
            </div>
    </form>
  </body>
</html>