<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {if $permissions['add_projects'] eq '1'}<button onclick="self.location.href=('{$editor}')">New {$title}</button>{/if} 
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}</th>
            {/foreach}
            {if $permissions['show_project_employees'] eq '1'}<th></th>{/if}
            {if $permissions['show_project_tasks'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['edit_projects'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['delete_projects'] eq '1'}<th class="th"></th>{/if}
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
          <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{$v['name']}</td>
            <td class="left_align">{$v['status']}</td>
            <td class="left_align">{$v['customers']}</td>
            <td class="left_align">{$v['description']}</td>
            <td class="left_align">{$v['start_date']}</td>
            <td class="left_align">{if $v['deadline'] eq '0000-00-00'}-{else}{$v['deadline']}{/if}</td>
             {if $permissions['show_project_employees'] eq '1'}<td class="centered"><a href="?menu=employee_project&id={$v['id']}"><button>Employees ({$v['employees_count']})</button></a></td>{/if}
             {if $permissions['show_project_tasks'] eq '1'}<td class="centered"><a href="?menu=task&project_id={$v['id']}"><button>Tasks ({$v['task_count']})</button></a></td>{/if}
             {if $permissions['edit_projects'] eq '1'}<td class="centered"><a href="?menu={$title}_editor&id={$v['id']}"><button class="edit"></button></a></td>{/if}
             {if $permissions['delete_projects'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu={$title}&id={$v['id']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
  </body>
</html>