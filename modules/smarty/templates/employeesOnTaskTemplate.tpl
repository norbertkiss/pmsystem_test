<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {foreach from=$warning item=value}
        <span style="color:red">{$value}</span>
    {/foreach}
    
    {if $permissions['add_task_employees'] eq '1'}<button onclick="self.location.href=('?menu=add_employee_to_task&id={$id}{if $project_id neq ""}&project_id={$project_id}{/if}')">Add employee</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}</th>
            {/foreach}
            {if $permissions['delete_task_employees'] eq '1'}<th></th>{/if}
            
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            <td>{$v['id']}</td>
            <td>{$v['name']}</td>
            {if $permissions['delete_task_employees'] eq '1'}<td><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=employees_on_task&id={$id}&employee={$v['name']}&action=delete{if $project_id neq ''}&project_id={$project_id}{/if}"><button>Delete</button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
    <a href="?menu=task{if $project_id neq ''}&project_id={$project_id}{/if}"><button>Back</button></a>
  </body>
</html>