<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&action=submit{if $main_project_id neq ''}&project_id={$main_project_id}{/if}" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update&project_id={$main_project_id}" method="post" class="basic-grey">
    {/if}
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      {if $id eq '' && $main_project_id eq ''}
      <label>   
          <span>Project :</span>
          <select required name="project_name" id="project_name">
            {if $project_name eq ''}
            <option selected="">Select...</option>
            {else}
            <option selected="{$project_name}">{$project_name}</option>
            {/if}
            {foreach from=$projects item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      {/if}
      
      <label>
          <span>Name :</span>
          <input id="name" required type="text" name="name" placeholder="Task's name" value="{$name}" />
      </label>
      
        <label>   
            <span>Priority :</span>
            <select required name="priority" id="priority">
              {if $priority eq ''}
              <option selected="">Select...</option>
              {else}
              <option selected="{$priority}">{$priority}</option>
              {/if}
              <option value="Low">Low</option>
              <option value="Medium">Medium</option>
              <option value="High">High</option>
            </select>
        </label>
      
      {if $id neq ''}
        <label>   
            <span>Status :</span>
            <select required name="status" id="status">
              {if $status eq ''}
              <option selected="">Select...</option>
              {else}
              <option selected="{$status}">{$status}</option>
              {/if}
              <option value="In Progress">In Progress</option>
              <option value="Canceled">Canceled</option>
              <option value="Completed">Completed</option>
              <option value="On Hold">On Hold</option>
            </select>
        </label>
      {/if}
      
      <label>
          <span>Deadline :</span>
          <input id="deadline" type="date" name="deadline"  value="{$deadline}" />
      </label>
          
      
      <label>
          <span>Description :</span>
          <input type="text" id="description" name="description" placeholder="Task's description" value="{$description}"/>
      </label>
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=task{if $main_project_id neq ''}&project_id={$main_project_id}{/if}">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>