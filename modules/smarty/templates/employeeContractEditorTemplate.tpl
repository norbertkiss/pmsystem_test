<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
     </head>
      <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=submit" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update" method="post" class="basic-grey">
    {/if}
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>
          <span>Salary :</span>
          <input id="salary" required type="number" name="salary"  value="{$salary}" />
      </label>
      <label>
          <span>Free Days :</span>
          <input id="free_days" required type="number" name="free_days"  value="{$free_days}" />
      </label>
      <label>
          <span>Start Date :</span>
          <input id="start_date" required type="date" name="start_date"  value="{$start_date}" />
      </label>
      <label>
          <span>Expiration :</span>
          <input id="expiration_date" required type="date" name="expiration_date"  value="{$expiration_date}" />
      </label>
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=employee">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>