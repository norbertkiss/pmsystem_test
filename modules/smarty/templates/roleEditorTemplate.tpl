<html>
  <head>
    <title>Role</title>
    <script language="javascript">
      
      function checkAll(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 0; i < checkboxes.length; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 0; i < checkboxes.length; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }  
      
     {* function checkUsers(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 3; i < 12; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 3; i < 12; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
    
      function checkRoles(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 12; i < 21; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 12; i < 21; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
    
    function checkEmployees(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 21; i < 42; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 21; i < 42; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
    
    function checkPositions(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 42; i < 49; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 42; i < 49; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
    
    function checkProjects(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 49; i < 68; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 49; i < 68; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }
    
    function checkTasks(ele) {
     var checkboxes = document.getElementsByTagName('input');
     if (ele.checked) {
         for (var i = 68; i < 89; i++) {
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = true;
             }
         }
     } else {
         for (var i = 68; i < 89; i++) {
             console.log(i)
             if (checkboxes[i].type == 'checkbox') {
                 checkboxes[i].checked = false;
             }
         }
     }
    }*}
    
    
    
    
      
    </script>
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="?menu=role_editor&action=submit" method="post">
    {elseif $page eq 'edit'}
    <form action="?menu=role_editor&id={$id}&action=update" method="post">
    {/if}
    
    <h1>Role Form </h1>
    Please fill all the texts in the fields.
          
          {foreach from=$warning item=value}
            <br><span style="color:red"><br>{$value}</span>
          {/foreach}
            <br><span style="color:green">{$confirm}</span>
  
    <br>
    <label>
        <span>Name :</span>
        <input required id="name" type="text" name="name" value="{$name}"/>
    </label>
    
    <br><br><fieldset>
    <legend>Assign roles:</legend>
    <label><INPUT type="checkbox" onchange="checkAll(this)" name="chk[]" /><b> Select all</b></label>
    <br><br><label><INPUT type="checkbox" onchange="checkUsers(this)" name="chk[]" /><b> USER</b></label>
    <label><input type='hidden' value='0' name='show_users'>
    <br><label><input id="show_users" type="checkbox" name="show_users" value="1" {if $show_users eq '1'}checked{/if}/>Show users</label>
    <label><input type='hidden' value='0' name='edit_users'>
    <br><label><input id="edit_users" type="checkbox" name="edit_users" value="1" {if $edit_users eq '1'}checked{/if}/>Edit users</label>
    <label><input type='hidden' value='0' name='add_users'>
    <br><label><input id="add_users" type="checkbox" name="add_users" value="1" {if $add_users eq '1'}checked{/if}/>Add users</label>
    <label><input type='hidden' value='0' name='delete_users'>
    <br><label><input id="delete_users" type="checkbox" name="delete_users" value="1" {if $delete_users eq '1'}checked{/if}/>Delete users</label>
    <label><input type='hidden' value='0' name='show_user_roles'>
    <br><label><input id="show_user_roles" type="checkbox" name="show_user_roles" value="1" {if $show_user_roles eq '1'}checked{/if}/>Show user's roles</label>
    <label><input type='hidden' value='0' name='add_user_roles'>
    <br><label><input id="add_user_roles" type="checkbox" name="add_user_roles" value="1" {if $add_user_roles eq '1'}checked{/if}/>Add role to users</label>
    <label><input type='hidden' value='0' name='delete_user_roles'>
    <br><label><input id="delete_user_roles" type="checkbox" name="delete_user_roles" value="1" {if $delete_user_roles eq '1'}checked{/if}/>Delete user's roles</label>
    
    
    <br><br><label><INPUT type="checkbox" onchange="checkRoles(this)" name="chk[]" /><b> ROLE</b></label>
    <label><input type='hidden' value='0' name='show_roles'>
    <br><label><input id="show_roles" type="checkbox" name="show_roles" value="1" {if $show_roles eq '1'}checked{/if}/>Show roles</label>
    <label><input type='hidden' value='0' name='edit_roles'>
    <br><label><input id="edit_roles" type="checkbox" name="edit_roles" value="1" {if $edit_roles eq '1'}checked{/if}/>Edit roles</label>
    <label><input type='hidden' value='0' name='add_roles'>
    <br><label><input id="add_roles" type="checkbox" name="add_roles" value="1" {if $add_roles eq '1'}checked{/if}/>Add roles</label>
    <label><input type='hidden' value='0' name='delete_roles'>
    <br><label><input id="delete_roles" type="checkbox" name="delete_roles" value="1" {if $delete_roles eq '1'}checked{/if}/>Delete roles</label>
    
    <br><br><label><INPUT type="checkbox" onchange="checkEmployees(this)" name="chk[]" /><b> EMPLOYEE</b></label>
    <label><input type='hidden' value='0' name='show_employees'>
    <br><label><input id="show_employees" type="checkbox" name="show_employees" value="1" {if $show_employees eq '1'}checked{/if}/>Show employees</label>
    <label><input type='hidden' value='0' name='add_employees'>
    <br><label><input id="add_employees" type="checkbox" name="add_employees" value="1" {if $add_employees eq '1'}checked{/if}/>Add employees</label>
    <label><input type='hidden' value='0' name='show_employee_contracts'>
    <br><label><input id="show_employee_contracts" type="checkbox" name="show_employee_contracts" value="1" {if $show_employee_contracts eq '1'}checked{/if}/>Show employee's contracts</label>
    <label><input type='hidden' value='0' name='add_employee_contracts'>
    <br><label><input id="add_employee_contracts" type="checkbox" name="add_employee_contracts" value="1" {if $add_employee_contracts eq '1'}checked{/if}/>Add contract to employees</label>
    <label><input type='hidden' value='0' name='delete_employee_contracts'>
    <br><label><input id="delete_employee_contracts" type="checkbox" name="delete_employee_contracts" value="1" {if $delete_employee_contracts eq '1'}checked{/if}/>Delete employee's contracts</label>
    <label><input type='hidden' value='0' name='show_employee_positions'>
    <br><label><input id="show_employee_positions" type="checkbox" name="show_employee_positions" value="1" {if $show_employee_positions eq '1'}checked{/if}/>Show employee's position</label>
    <label><input type='hidden' value='0' name='add_employee_positions'>
    <br><label><input id="add_employee_positions" type="checkbox" name="add_employee_positions" value="1" {if $add_employee_positions eq '1'}checked{/if}/>Add position to employees</label>
    <label><input type='hidden' value='0' name='delete_employee_positions'>
    <br><label><input id="delete_employee_positions" type="checkbox" name="delete_employee_positions" value="1" {if $delete_employee_positions eq '1'}checked{/if}/>Delete employee's position</label>
    <label><input type='hidden' value='0' name='edit_employees'>
    <br><label><input id="edit_employees" type="checkbox" name="edit_employees" value="1" {if $edit_employees eq '1'}checked{/if}/>Edit employees</label>
    <label><input type='hidden' value='0' name='delete_employees'>
    <br><label><input id="delete_employees" type="checkbox" name="delete_employees" value="1" {if $delete_employees eq '1'}checked{/if}/>Delete employees</label>
    
    <br><br><label><INPUT type="checkbox" onchange="checkPositions(this)" name="chk[]" /><b> POSITION</b></label>
    <label><input type='hidden' value='0' name='show_positions'>
    <br><label><input id="show_positions" type="checkbox" name="show_positions" value="1" {if $show_positions eq '1'}checked{/if}/>Show positions</label>
    <label><input type='hidden' value='0' name='add_positions'>
    <br><label><input id="add_positions" type="checkbox" name="add_positions" value="1" {if $add_positions eq '1'}checked{/if}/>Add positions</label>
    <label><input type='hidden' value='0' name='edit_positions'>
    <br><label><input id="edit_positions" type="checkbox" name="edit_positions" value="1" {if $edit_positions eq '1'}checked{/if}/>Edit positions</label>
    <label><input type='hidden' value='0' name='delete_positions'>
    <br><label><input id="delete_positions" type="checkbox" name="delete_positions" value="1" {if $delete_positions eq '1'}checked{/if}/>Delete positions</label>
    
    <br><br><label><INPUT type="checkbox" onchange="checkProjects(this)" name="chk[]" /><b> PROJECT</b></label>
    <label><input type='hidden' value='0' name='show_projects'>
    <br><label><input id="show_projects" type="checkbox" name="show_projects" value="1" {if $show_projects eq '1'}checked{/if}/>Show projects</label>
    <label><input type='hidden' value='0' name='show_all_projects'>
    <br><label><input id="show_all_projects" type="checkbox" name="show_all_projects" value="1" {if $show_all_projects eq '1'}checked{/if}/>Show all projects</label>
    <label><input type='hidden' value='0' name='add_projects'>
    <br><label><input id="add_projects" type="checkbox" name="add_projects" value="1" {if $add_projects eq '1'}checked{/if}/>Add projects</label>
    <label><input type='hidden' value='0' name='edit_projects'>
    <br><label><input id="edit_projects" type="checkbox" name="edit_projects" value="1" {if $edit_projects eq '1'}checked{/if}/>Edit projects</label>
    <label><input type='hidden' value='0' name='delete_projects'>
    <br><label><input id="delete_projects" type="checkbox" name="delete_projects" value="1" {if $delete_projects eq '1'}checked{/if}/>Delete projects</label>
    <label><input type='hidden' value='0' name='show_project_employees'>
    <br><label><input id="show_project_employees" type="checkbox" name="show_project_employees" value="1" {if $show_project_employees eq '1'}checked{/if}/>Show project's employees</label>
    <label><input type='hidden' value='0' name='add_project_employees'>
    <br><label><input id="add_project_employees" type="checkbox" name="add_project_employees" value="1" {if $add_project_employees eq '1'}checked{/if}/>Add employee to project</label>
    <label><input type='hidden' value='0' name='delete_project_employees'>
    <br><label><input id="delete_project_employees" type="checkbox" name="delete_project_employees" value="1" {if $delete_project_employees eq '1'}checked{/if}/>Delete employee from project</label>
    <label><input type='hidden' value='0' name='show_project_tasks'>
    <br><label><input id="show_project_tasks" type="checkbox" name="show_project_tasks" value="1" {if $show_project_tasks eq '1'}checked{/if}/>Show project's tasks</label>
    
    <br><br><label><INPUT type="checkbox" onchange="checkTasks(this)" name="chk[]" /><b> TASK</b></label>
    <label><input type='hidden' value='0' name='show_tasks'>
    <br><label><input id="show_tasks" type="checkbox" name="show_tasks" value="1" {if $show_tasks eq '1'}checked{/if}/>Show tasks</label>
    <label><input type='hidden' value='0' name='show_all_tasks'>
    <br><label><input id="show_all_tasks" type="checkbox" name="show_all_tasks" value="1" {if $show_all_tasks eq '1'}checked{/if}/>Show all tasks</label>
    <label><input type='hidden' value='0' name='add_tasks'>
    <br><label><input id="add_tasks" type="checkbox" name="add_tasks" value="1" {if $add_tasks eq '1'}checked{/if}/>Add tasks</label>
    <label><input type='hidden' value='0' name='edit_tasks'>
    <br><label><input id="edit_tasks" type="checkbox" name="edit_tasks" value="1" {if $edit_tasks eq '1'}checked{/if}/>Edit tasks</label>
    <label><input type='hidden' value='0' name='delete_tasks'>
    <br><label><input id="delete_tasks" type="checkbox" name="delete_tasks" value="1" {if $delete_tasks eq '1'}checked{/if}/>Delete tasks</label>
    <label><input type='hidden' value='0' name='show_task_start'>
    <br><label><input id="show_task_start" type="checkbox" name="show_task_start" value="1" {if $show_task_start eq '1'}checked{/if}/>Show task's start button</label>
    <label><input type='hidden' value='0' name='show_task_stop'>
    <br><label><input id="show_task_stop" type="checkbox" name="show_task_stop" value="1" {if $show_task_stop eq '1'}checked{/if}/>Show task's stop button</label>
    <label><input type='hidden' value='0' name='show_task_employees'>
    <br><label><input id="show_task_employees" type="checkbox" name="show_task_employees" value="1" {if $show_task_employees eq '1'}checked{/if}/>Show task's employees</label>
    <label><input type='hidden' value='0' name='add_task_employees'>
    <br><label><input id="add_task_employees" type="checkbox" name="add_task_employees" value="1" {if $add_task_employees eq '1'}checked{/if}/>Add employee to task</label>
    <label><input type='hidden' value='0' name='delete_task_employees'>
    <br><label><input id="delete_task_employees" type="checkbox" name="delete_task_employees" value="1" {if $delete_task_employees eq '1'}checked{/if}/>Delete task's employees</label>
    
    <label><input type='hidden' value='0' name='edit_profile'>
    <br><br><label><input id="edit_profile" type="checkbox" name="edit_profile" value="1" {if $edit_profile eq '1'}checked{/if}/>Edit profile</label>
    
    <br><br><input type="submit" value="Submit">
    </form>
    </fieldset>
    <a href="?menu=role"><button>Back</button></a>
    
    
    