<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    <br>
    {foreach from=$warning item=value}
        <span style="color:red">{$value}</span>
    {/foreach}<br>
    
    {if $permissions['add_project_employees'] eq '1'}<button onclick="self.location.href=('?menu=add_employee_to_project&id={$id}')">Add employee</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}</th>
            {/foreach}
            {if $permissions['delete_project_employees'] eq '1'}<th></th>{/if}
            
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{$v['name']}</td>
            {if $permissions['delete_project_employees'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=employee_project&id={$id}&employee={$v['name']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
    <a href="?menu=project"><button>Back</button></a>
  </body>
</html>