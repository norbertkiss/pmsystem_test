<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    
    {if $permissions['add_employee_contracts'] eq '1'}<button onclick="self.location.href=('{$editor}&id={$id}')">Add {$title}</button> {/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}</th>
            {/foreach}
            {if $permissions['delete_employee_contracts'] eq '1'}<th></th>{/if}
            
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            <td class="left_align">{$v['employee_id']}</td>
            <td class="left_align">{$v['salary']} {$currency}</td>
            <td class="left_align">{$v['free_days']}</td>
            <td class="left_align">{$v['start_date']}</td>
            <td class="left_align">{$v['expiration_date']}</td>
            {if $permissions['delete_employee_contracts'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu={$title}&id={$id}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
    <a href="?menu=employee"><button>Back</button></a>
  </body>
</html>