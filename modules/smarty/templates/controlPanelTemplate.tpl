<html>
<head>
     <link rel="stylesheet" href="modules/css/cpanel.css" type="text/css" />
</head>
<body>

<div class="panel">
    <div class="welcome">
        <h1>Welcome {$user}!</h1>
    </div>
        <div class="logout">    
            <ul>  
                 {if $permissions['edit_profile'] eq '1'}
                <li><a href="?menu=profile"><input class="c_submit" name="submit" type="submit" value="Profile"></a></li>
                {/if}
                <li class="logout_btn">
                    <form action="" method="post">
                        <input class="c_submit" name="submit" type="submit" value="Logout">
                    </form>
                </li>
            </ul>
        </div>
                
</div>
<br>
</body>
</html>