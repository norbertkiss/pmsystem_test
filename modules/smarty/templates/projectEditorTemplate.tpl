<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    {if $page eq 'new'}
    <form action="{$SCRIPT_NAME}&action=submit" method="post" class="basic-grey">
    {elseif $page eq 'edit'}
    <form action="{$SCRIPT_NAME}&id={$id}&action=update" method="post" class="basic-grey">
    {/if}
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>   
          <span>Project manager :</span>
          <select required name="project_manager" id="project_manager">
            {if $project_manager eq ''}
            <option selected value="">Select...</option>
            {else}
            <option selected value="{$project_manager}">{$project_manager}</option>
            {/if}
            {foreach from=$project_managers item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      
      
      <label>
          <span>Name :</span>
          <input id="name" required type="text" name="name" placeholder="Project's name" value="{$name}" />
      </label>
      
      {if $id neq ''}
      <label>   
          <span>Status :</span>
          <select required name="status" id="status">
            {if $status eq ''}
            <option selected="">Select...</option>
            {else}
            <option selected="{$status}">{$status}</option>
            {/if}
            <option value="Active">Active</option>
            <option value="On Hold">On Hold</option>
            <option value="Canceled">Canceled</option>
            <option value="Finished">Finished</option>
          </select>
      </label>
      {/if}
      
      <label>
          <span>Customers :</span>
          <input type="text" id="customers" required name="customers" placeholder="Project's customers" value="{$customers}"/>
      </label>
      
      <label>
          <span>Deadline :</span>
          <input id="deadline" type="date" name="deadline"  value="{$deadline}" />
      </label>
      
      <label>
          <span>Description :</span>
          <input type="text" id="description" required name="description" placeholder="Project's description" value="{$description}"/>
      </label>
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=project">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>