<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/profile.css">
</head>
  <body>
  
     <div class="wrapper">   
        <div class="profile">
                <form action="{$SCRIPT_NAME}&action=submit" method="post">
                            <div class="header">
                                <div class="title">
                                    <h1>{$Utitle} Profile </h1>
                                    <span>Please fill all the texts in the fields.</span>
                                </div>
                                    <div class="avatar"></div>
                                    {foreach from=$warning item=value}
                                      <div class = "warning"><br>{$value}</div>
                                    {/foreach}
                                    <div class = "confirmation">{$confirm}</div>
                                    
                            </div>    

                            <hr>
                                <label>
                                    <span>Username :</span>
                                    <input id="username"  type="text" name="username" value="{$username}" />
                                </label>

                                <label>
                                    <span>Password :</span>
                                    <input id="password" required type="password" name="password" value="{$password}" />
                                </label>

                                <label>
                                    <span>New Password :</span>
                                    <input id="new_password"  type="password" name="new_password" value="{$new_password}" />
                                </label>

                                <label>
                                    <span>Confirm Password :</span>
                                    <input id="re_password"  type="password" name="re_password" value="{$re_password}" />
                                </label>
                            

                            <div class="buttons">  
                                <div class="button">
                                    <input type="submit" class="btn_submit" value="Submit"/>
                                    <a class="btn_back" href="?menu=">Back</a>
                                </div>
                            </div>
                            
                </form>
        </div>
    </div>
  </body>
</html>
