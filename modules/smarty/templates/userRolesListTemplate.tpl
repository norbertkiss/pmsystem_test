<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    {if $permissions['add_user_roles'] eq '1'}<button onclick="self.location.href=('?menu=user_roles_editor&id={$id}')">Add role</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            {if $permissions['delete_user_roles'] eq '1'}<th></th>{/if}
            
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
          <tr>
            <td class="left_align">{$v['id']}</td>
            <td class="left_align">{$v['name']}</td>
            {if $permissions['delete_user_roles'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=user_roles&id={$id}&delete_id={$v['id']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
        <a href="?menu=user"><button>Back</button></a>
  </body>
</html>