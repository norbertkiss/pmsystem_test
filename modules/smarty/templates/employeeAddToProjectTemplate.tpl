<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" type="text/css" href="modules/css/editor.css">
  </head>
  <body>
    
    
    <form action="?menu=add_employee_to_project&id={$id}&action=submit" method="post" class="basic-grey">
    
      <h1>{$Utitle} Form 
          <span><br>Please fill all the texts in the fields.</span>
          {foreach from=$warning item=value}
            <div class = "warning"><br>{$value}</div>
          {/foreach}
          <div class = "confirmation">{$confirm}</div>
      </h1>
      <hr>
      <label>   
          <span>Employee :</span>
          <select required name="employee" id="employee">
            
            <option selected value="">Select...</option>
            
        
            {foreach from=$employees item=adatok}
              {foreach from=$adatok item=nev}
                <option value="{$nev}">{$nev}</option>
              {/foreach}
            {/foreach}
          </select>
      </label>
      
     
      
        <div class="buttons">  
            <div class="button">
                <input type="submit" class="btn_submit" value="Submit"/>
                <a class="btn_back" href="?menu=employee_project&id={$id}">Back</a>
            </div>
        </div>
    </form>
  </body>
</html>