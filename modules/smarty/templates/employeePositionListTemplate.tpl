<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
    
    
    {if $permissions['add_employee_positions'] eq '1'}<button onclick="self.location.href=('{$editor}&id={$id}')">Add {$title}</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th>{$value}</th>
            {/foreach}
            {if $permissions['delete_employee_positions'] eq '1'}<th></th>{/if}
            
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            {foreach from=$v item=value}
                <td class="left_align">{$value}</td>
            {/foreach}
            
            {if $permissions['delete_employee_positions'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=employee_position&id={$id}&p_name={$v['Name']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
    <a href="?menu=employee"><button>Back</button></a>
  </body>
</html>