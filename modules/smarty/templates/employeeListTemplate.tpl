<html>
  <head>
    <title>{$title}</title>
    <link rel="stylesheet" type="text/css" href="modules/css/list.css">
  </head>
  <body>
   
    {if $permissions['add_employees'] eq '1'}<button onclick="self.location.href=('{$editor}')">New {$title}</button>{/if}
    
    <table class = "bordered">
        <thead>
        <tr>
            {foreach from=$headers item=value}
                <th >{$value}</th>
            {/foreach}
            {if $permissions['show_employee_contracts'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['show_employee_positions'] eq '1'}<th class="th"></th>{/if}
             {if $permissions['edit_employees'] eq '1'}<th class="th"></th>{/if}
            {if $permissions['delete_employees'] eq '1'}<th class="th"></th>{/if}
        </tr>
        </thead>
        
        {foreach from=$data key=k item=v}
        
        <tr>
            {foreach from=$v item=value}
                <td class="left_align">{$value}</td>
            {/foreach}
            {if $permissions['show_employee_contracts'] eq '1'}<td class="centered"><a href="?menu=contract&id={$v['id']}"><button>Contract</button></a></td>{/if}
            {if $permissions['show_employee_positions'] eq '1'}<td class="centered"><a href="?menu=employee_position&id={$v['id']}"><button>Position</button></a></td>{/if}
            {if $permissions['edit_employees'] eq '1'}<td class="centered"><a href="?menu={$title}_editor&id={$v['id']}"><button class="edit"></button></a></td>{/if}
            {if $permissions['delete_employees'] eq '1'}<td class="centered"><a onclick="return confirm('Are you sure you want to delete this case?')" href="?menu=employee&id={$v['id']}&action=delete"><button class="delete"></button></a></td>{/if}
        </tr>
        {/foreach}
    
    </table>
  </body>
</html>
