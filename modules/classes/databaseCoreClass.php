<?php


class Database extends PDO {
    
    
	function __construct($user, $password, $location, $dbname) {
		
            $dsn = 'mysql:dbname=' . $dbname . ';host=' . $location.';charset=utf8';
            
            try {
		
                parent::__construct($dsn, $user, $password);
                $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
            }
            
            catch(PDOException $e) {
		
                //echo 'Connection failed: ' . $e->getMessage();
				error_log($e->getMessage(),0);
		
            }
                        
        }
	
}


         