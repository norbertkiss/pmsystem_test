<?php

class Mysql_Database extends Database{
	protected $user, $password, $location, $dbname;
	protected $resource = null;
	static private $instance;
	
	static $datatypes = array ('VAR_STRING' => 'string',
				    'STRING' => 'string',
					'NEWDECIMAL' => 'int',
				    'BLOB' => 'string',
				    'LONGLONG' => 'int',
				    'LONG' => 'int',
				    'SHORT' => 'int',
				    'DATETIME' => 'datetime',
				    'DATE' => 'date',
				    'DOUBLE' => 'real',
				    'TIMESTAMP' => 'timestamp');

	static function getInstance() {
		if (!isset(self::$instance)) { 
			self::$instance = new self(DBConfig::$user, DBConfig::$password,
					DBConfig::$location, DBConfig::$dbname);
			self::$instance->connect();
		}

		return self::$instance;      
	}

	function __construct($user, $password, $location, $dbname) {
		$this->user = $user;
		$this->password = $password;
		$this->location = $location;
		$this->dbname = $dbname;
		
	}

	function connect(){
		if (isset($this->resource)) return false;

		if (!($this->resource = new Database($this->user, $this->password, $this->location, $this->dbname))) {
			throw new Exception("Failed to connect to database!");
		}
    
		return true;
	}
	
	function query($sql,array $data=NULL) {
		
		$this->resource->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
		
		if(empty($data)){
			
			try {
			    $stmt = $this->resource->prepare($sql);
			    $stmt->execute();
			    
			}
			catch (PDOException $e)
			{
			    echo 'Connection failed: ' .$e->getMessage();
			    return false;
			}
			
			return $stmt;
		
			}
		
		else {
		
			try {
				
				$stmt = $this->resource->prepare($sql);
				if ($stmt->execute($data)){
					//echo 'Done';
				}
		    
			}
			catch (PDOException $e)
			{
			    echo 'Connection failed: ' .$e->getMessage();
			    return false;
			}
			
			return $stmt;	
			
		}
	}
	
	
	function select($sql) {
		
		$STH = $this->resource->query($sql); 

		$STH->setFetchMode(PDO::FETCH_ASSOC);
		 
		while($row = $STH->fetch()) {
		    $result[] = $row;
		    
		}
		
		return $result;	
		
	}
	
	function columnExists ($column, $table){
        
		$q = $this->resource->prepare("DESCRIBE $table");
		$q->execute();
		$table_fields = $q->fetchAll(PDO::FETCH_COLUMN);
		
		foreach($table_fields as $field_name){
		    if ($field_name === $column){
			return true;
		    }
		}
		
		return false;
		
		
		
	}
	
	function getLastInsertID(){
		$id = $this->resource->lastInsertId();
		return $id;
	}
  
	
  
	
}



