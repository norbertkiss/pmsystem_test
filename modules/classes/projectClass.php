<?php

class Project {
    
    private $id;
    private $table;
    private $smarty;
	private $permissions;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('project');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    }
    
    function create(array $datas){
       
        if($this->isUnique('name', $datas['name'])){
            $datas['status'] = 'Active';
            return $this->table->insert($datas);
        
        }
        
        return false;
    }
    
    function getObjectData(){
        return $this->table->getRow($this->id);
        
    }
    
    
    function getData($employee_id=NULL){
		
		if ($this->permissions['show_all_projects']){
			
			$field = 'id';
			$orderby = 'id';
			$order_dir = 'ASC';
			$limit_start = '0';
			$limit_count = '9999';
			
			return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
		
		} else {
			
			$select = array('*');
			$from = array ('project');
			$where = array("id IN (SELECT project_id from e_project WHERE employee_id = '".$employee_id."')");
        
			$data = $this->table->selectBuilder($select,$from,$where);
			
			return $data;
			
		}
	
	
	}
	
	
	
    
    function getNames(){
        
        $table = new Table('project');
        return $table->getColumn('name');
        
    }
	
	function getEmployees($id){
		
		$select = array('name');
		$from = array ('employee');
		$where = array("id NOT IN (SELECT employee_id from e_project WHERE project_id = '".$id."')");
        
                $data = $this->table->selectBuilder($select,$from,$where);
		return $data;
		
		
	}
	
	
    
    function getTasksCount($id, $employee_id=NULL){
		
		if ($this->permissions['show_all_tasks']){
			//$select = array('name', 'position');
			$select = array('*');
			$from = array ('project', 'task');
			$where = array("task.project_id = project.id",
                                       "task.project_id = '".$id."'");
			
			$data = $this->table->selectBuilder($select,$from,$where);
		} else {
			
			$select = array('task.id', 'project_id', 'project.name AS pname',
                                'task.name AS tname', 'task.priority', 'task.status',
                                'task.start_date', 'task.complete_date', 'task.deadline', 'task.description');
			$from = array ('task', 'project');
			$where = array("task.project_id = project.id",
                                       "task.project_id = '".$id."'",
                                       "task.id IN (SELECT task_id from e_task WHERE employee_id = '".$employee_id."')");
        
			$data = $this->table->selectBuilder($select,$from,$where);
			
			
		}
		
		
		return count($data);
		
	}
	
	function getEmployeesCount($project_id){
		
		//$select = array('name', 'position');
		$select = array('*');
		$from = array ('project', 'e_project');
		$where = array("project.id = e_project.project_id",
					   "e_project.project_id = '".$project_id."'");
		
		$data = $this->table->selectBuilder($select,$from,$where);
		return count($data);
		
	}
        
        function employeeNotLinked($employee_id, $project_id){
            
            $select = array('*');
            $from = array ('e_project');
            $where = array("employee_id = '".$employee_id."'",
                           "project_id = '".$project_id."'");

            $data = $this->table->selectBuilder($select,$from,$where);
            
            if (empty($data)){
                return true;
            } else {
                return false;
            }
        }
	
    
    function getIdByName($name){
        
        $table = new Table('project');
        $id = $table->getValueByColumn('id', 'name', $name);
        return $id['id'];
    }
	

    
    function searchData($field,$value,$orderby,$direction,$limit_start,$limit_count){
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);    
    }
    
    
    
    function set(array $datas){
        
        $old_data = $this->getObjectData();
        
        if($old_data['name'] != $datas['name']){
        
            if($this->isUnique('name', $datas['name'])){
               
                return $this->table->update($this->id, $datas);
            
            }
        
        } else {
            
            return $this->table->update($this->id, $datas);
        }
        
        return false;
    }
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    function isUnique($unique_column, $data){
        $field = $unique_column;
        $value = $data;
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        if ($this->table->queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count)){
            return false;
        }
        
        return true;
    }
    
    function showList(){
        
        $classname = 'Project'; // CUSTOMIZABLE
        $title = 'project'; // CUSTOMIZABLE
        $headers = array('ID', 'Name', 'Status', 'Customers', 'Description', 'Start', 'Deadline'); // CUSTOMIZABLE
        $user = new User;
		
        if ($_GET['action'] === 'delete'){
            $delete_id = $_GET['id'];
            $field = new $classname($delete_id);
            $field->delete();  
        }
        //var_dump($this->getTasksCount('6'));
        $smarty = $this->smarty;
        $class = new $classname();
		
		if($user_id = $user->getIdByUsername($_SESSION['login_user'])){

                $employee_id = $user->getEmployeeIdByUserId($user_id);
				
		}
		//var_dump($employee_id);
        $data = $class->getData($employee_id);
        
        foreach ($data as $key => $item){            
            $data[$key]['task_count'] = $this->getTasksCount($item['id'], $employee_id);
        }
        
		foreach ($data as $key => $item){            
            $data[$key]['employees_count'] = $this->getEmployeesCount($item['id']);
        }
        
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('permissions', $this->permissions);
        $smarty->display($title."ListTemplate.tpl");
        
    }
    
    function showEditor(){
        
        $title = 'project'; //CUSTOMIZABLE
        $Utitle = 'Project'; //CUSTOMIZABLE
        $classname = 'Project'; //CUSTOMIZABLE
        $employee = new Employee;
        $project_managers = $employee->getEmployeesByPosition('Projekt Menedzser');
        //var_dump($project_managers);
		
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."_editor");
        $smarty->assign('Utitle', $Utitle);
		$smarty->assign('project_managers', $project_managers);
		
			 
        if(empty($_GET['id'])){
            $smarty->assign('page', 'new');
            $class = new $classname();
            
         
        } else {
            $id = $_GET['id'];
            $class = new $classname($id);
            $old_data = $class->getObjectData();
            //var_dump($old_data);
			
			
            if(empty($_POST)){
                $data = $class->getObjectData();
                $old_data = $data;
//                var_dump($old_data);
            } else {
                $data = $_POST;
            }
            //CUSTOMIZABLES
            $smarty->assign('id', $id);
            $smarty->assign('page', 'edit');
            $smarty->assign('name', $data['name']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('customers', $data['customers']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
			$smarty->assign('project_manager', $data['project_manager']);
			
        }
        
               
        if ($_GET['action'] === 'submit'){
            if(!empty($_POST)){
                $data = $_POST;
            }
            
            //CUSTOMIZABLES
            $smarty->assign('name', $data['name']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('customers', $data['customers']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
            $smarty->assign('id', $id);
			$smarty->assign('project_manager', $data['project_manager']);
        }
        
        if ($_GET['action'] === 'update'){
            
            //CUSTOMIZABLES
            $smarty->assign('name', $data['name']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('customers', $data['customers']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
			$smarty->assign('project_manager', $data['project_manager']);
            $smarty->assign('id', $id);
        }
        
     /******** Validalas ********/
    if(!empty($_GET['action'])){
		
		if (empty($_POST['project_manager'])){
                
            $warning[] = 'Please select a project manager.';
        
        }
            
        if (!Validation::validName($_POST['name'])){
                
            $warning[] = 'Invalid name!';
        
        }
        
        if (!Validation::validStatus($_POST['status'])){
                
            $warning[] = 'Invalid status!';
        
        }
        
        if (!Validation::validCustomers($_POST['customers'])){
                
            $warning[] = 'Invalid customer!';
        
        }
        
        if (!Validation::validDescription($_POST['description'])){
                
            $warning[] = 'Invalid description!';
        
        }
        
        if(empty($warning) && $_GET['action'] === 'submit' && !$class->isUnique('name', $_POST['name'])){
            
            $warning[] = 'Name already exists!';
            
        }
        
        if($_GET['action']==='update' && $_POST['name'] != $data['name']  && !$class->isUnique('name', $_POST['name'])){
       
            $warning[] = 'Name already exists!';
            
        }
        
    }
        
        
        // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
				//var_dump($_POST['project_manager']);
				$_POST['created_by'] = $_SESSION['login_user'];
				
                if($class->create($_POST)){
					
                    $project_id = $this->getIdByName($_POST['name']);
                    
                    $employee_id = $employee->getIdByName($_POST['project_manager']);
                    $employee-> createLinkProject($employee_id, $project_id);
					
                    $smarty->assign('confirm' , $Utitle." added successfully!");
                    
                }
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
				$_POST['updated_by'] = $_SESSION['login_user'];
				$_POST['last_update'] = date('Y-m-d G:i:s');
				$old_employee_id = $employee->getIdByName($old_data['project_manager']);
				$employee_id = $employee->getIdByName($_POST['project_manager']);
				$project_id = $_GET['id'];
				
				
                 if($class->set($_POST)){
			
                    if($this->employeeNotLinked($employee_id, $project_id)){
//                      $employee->deleteLinkProject($old_employee_id, $project_id);
                        $employee->createLinkProject($employee_id, $project_id);
                        
                    }
                    $smarty->assign('confirm', $Utitle." updated successfully!");
                }
                
            }
        
        $smarty->display($title."EditorTemplate.tpl");
        
    }
	
	function showEmployeeProjectList(){
		
		
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'project'; // CUSTOMIZABLE
        $headers = array('ID', 'Name'); // CUSTOMIZABLE
		$id = $_GET['id'];
		$class = new $classname();
        
        if ($_GET['action'] === 'delete'){
            $project = new Project($id);
			$employee_id = $class->getIdByName($_GET['employee']);
			$project_id = $_GET['id'];
			//var_dump($employee_id);
			//var_dump($project_id);
			$object_data = $project->getObjectData();
			$manager = $object_data['project_manager'];
			$name = $_GET['employee'];
			//var_dump($name);
			
			if ($manager === $name){
				$warning[] = "Sorry, you can't delete the Project Manager";
			} else {
				$class->deleteLinkProject($employee_id, $project_id);
			}
        }
        //var_dump($this->getTasksCount('6'));
		$project_id = $_GET['id'];
        $smarty = $this->smarty;
        
        $data = $class->getEmployeesOnProject($project_id);
		//var_dump($data);
        
        //foreach ($data as $key => $item){            
        //    $data[$key]['task_count'] = $this->getTasksCount($item['id']);
        //}
        
		
		
        $smarty->assign('warning', $warning);
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
		$smarty->assign('id', $_GET['id']);
        $smarty->assign('title', $title);
        $smarty->assign('permissions', $this->permissions);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->display("employeeProjectListTemplate.tpl");
        
    
		
		
	}
	
	function showEmployeeProjectAdd(){
		
		
		$title = 'employee'; //CUSTOMIZABLE
        $Utitle = 'Employee'; //CUSTOMIZABLE
        $classname = 'Project'; //CUSTOMIZABLE
		$employee = new Employee;
		$id = $_GET['id'];
		$class = new $classname;
        
		
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."_editor");
        $smarty->assign('Utitle', $Utitle);
		$smarty->assign('project_managers', $project_managers);
		
		
		$data = $class->getEmployees($id);
		$smarty->assign('employees', $data);
		$smarty->assign('id', $id);
		
		
     /******** Validalas ********/
    
        
        
        // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                	
				$project_id = $id;
				$employee_id = $employee->getIdByName($_POST['employee']);
				
				if($employee-> createLinkProject($employee_id, $project_id)){
				
					$data = $class->getEmployees($id);
					$smarty->assign('employees', $data);
					$smarty->assign('confirm' , $Utitle." added successfully!");
				}
                
            } 
                    
        $smarty->display("employeeAddToProjectTemplate.tpl");
		
	}
    
}
    


 
  
  
  
  
  


