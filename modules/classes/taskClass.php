<?php

class Task {
    
    private $id;
    private $table;
    private $smarty;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('task');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    }
    
    function create(array $datas){
        
        $datas['status'] = 'Active';   
        return $this->table->insert($datas);
         
    }
    
    function getObjectData(){
        return $this->table->getRow($this->id);
        
    }
    
    function getJoinedData(){
        
        $select = array ('task.id', 'project_id', 'project.name AS pname',
                         'task.name AS tname', 'task.priority', 'task.status',
                         'task.start_date', 'task.complete_date', 'task.deadline', 'task.description');
        
        $tabla_ketto = 'project';
        $kulcs_egy = 'project_id';
        $kulcs_ketto = 'id';
        $oszlop = 'task.id';
        
        return $this->table->queryJoinedObjects($select, $tabla_ketto,$kulcs_egy,$kulcs_ketto,$oszlop,$ertek);
        
    }
    
    function getTasksForEmployee($employee_id){
		
		$select = array('task.id', 'project_id', 'project.name AS pname',
                                'task.name AS tname', 'task.priority', 'task.status',
                                'task.start_date', 'task.complete_date', 'task.deadline', 'task.description');
		$from = array ('task', 'project');
		$where = array("task.project_id = project.id",
                               "task.id IN (SELECT task_id from e_task WHERE employee_id = '".$employee_id."')");
        
        $data = $this->table->selectBuilder($select,$from,$where);
		return $data;
		
		
	}
    
    function getTasksOnProject($id, $employee_id=NULL){
        
        if ($this->permissions['show_all_tasks']){
        
            $select = array ('task.id', 'project_id', 'project.name AS pname',
                             'task.name AS tname', 'task.priority', 'task.status',
                             'task.description','task.start_date','task.deadline');

            $tabla_ketto = 'project';
            $kulcs_egy = 'project_id';
            $kulcs_ketto = 'id';
            $oszlop = 'project_id';

            return $this->table->queryJoinedUniqueObjects($select, $tabla_ketto,$kulcs_egy,$kulcs_ketto,$oszlop,$id);
        } else {
            
//            var_dump($id);
            $select = array('task.id', 'project_id', 'project.name AS pname',
                            'task.name AS tname', 'task.priority', 'task.status',
                            'task.start_date', 'task.complete_date', 'task.deadline', 'task.description');
            $from = array ('task', 'project');
            $where = array("task.project_id = project.id",
                           "task.project_id = '".$id."'",
                           "task.id IN (SELECT task_id from e_task WHERE employee_id = '".$employee_id."')");
        
            $data = $this->table->selectBuilder($select,$from,$where);
            return $data;
            
        }
        
        
    }
    
    function getEmployees($task_id, $project_id){
		
		$select = array('employee.name');
		$from = array ('employee', 'e_project');
		$where = array("employee.id = e_project.employee_id",
                       "e_project.project_id = '$project_id'",
                       "employee.id NOT IN (SELECT employee_id from e_task WHERE task_id = '".$task_id."')");
        
        $data = $this->table->selectBuilder($select,$from,$where);
		return $data;
		
		
	}
    
    function getEmployeesCount($task_id){
		
		//$select = array('name', 'position');
		$select = array('*');
		$from = array ('task', 'e_task');
		$where = array("task.id = e_task.task_id",
					   "e_task.task_id = '".$task_id."'");
		
		$data = $this->table->selectBuilder($select,$from,$where);
		return count($data);
		
	}
    
    
    
    function createCurrentTimestamp(){
        
        $date = date('Y-m-d G:i:s');
        return $date;
        
    }
    
    
    
    function getData(){
        $field = 'id';
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
    }
    
    function searchData($field,$value,$orderby,$direction,$limit_start,$limit_count){
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);    
    }
    
    function set(array $datas){
           
        return $this->table->update($this->id, $datas);
            
    }
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    function isUnique($unique_column, $data){
        $field = $unique_column;
        $value = $data;
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        if ($this->table->queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count)){
            return false;
        }
        
        return true;
    }
    
    function showList(){
       
        $classname = 'Task'; // CUSTOMIZABLE
        $title = 'task'; // CUSTOMIZABLE
        $headers = array('ID','Project Name', 'Name', 'Priority',
                         'Status', 'Description', 'Started', 'Completed', 'Deadline'); // CUSTOMIZABLE
        $user = new User;
        
        if ($_GET['action'] === 'delete'){
            $delete_id = $_GET['id'];
            $field = new $classname($delete_id);
            $field->delete();  
        }
        
        if ($_GET['action'] === 'start'){
            $id = $_GET['id'];
            $class = new $classname($id);
            $date = $this->createCurrentTimestamp();
            $datas = array ('start_date'=>"$date");
            $class->set($datas);
        }
        
        if ($_GET['action'] === 'reset'){
            $id = $_GET['id'];
            $class = new $classname($id);
            $date = "0000-00-00 00:00:00";
            $datas = array ('start_date'=>"$date");
            $class->set($datas);
        }
		
		if ($_GET['action'] === 'finish'){
            $id = $_GET['id'];
            $class = new $classname($id);
            $complete_date = $this->createCurrentTimestamp();
            $datas = array ('complete_date'=> $complete_date,
							'status' => 'Completed');
            $class->set($datas);
        }
        
        $smarty = $this->smarty;
        $class = new $classname();
        
        
        if (empty($_GET['project_id'])){
            
            if($this->permissions['show_all_tasks']){
                $data = $class->getJoinedData();
            } else {
                $user_id = $user->getIdByUsername($_SESSION['login_user']);

                $employee_id = $user->getEmployeeIdByUserId($user_id);
                
                $data = $class->getTasksForEmployee($employee_id);
            }
            
        } else {
                
		if($user_id = $user->getIdByUsername($_SESSION['login_user'])){

			$employee_id = $user->getEmployeeIdByUserId($user_id);
		}
                $project_id = $_GET['project_id']; 
                $data = $class->getTasksOnProject($_GET['project_id'], $employee_id);
             
                 
        }
             
        
        
        foreach ($data as $key => $item){            
            $data[$key]['employee_count'] = $this->getEmployeesCount($item['id']);
        }
        
        
        $smarty->assign('project_id', $project_id);
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('permissions', $this->permissions);
        $smarty->display($title."ListTemplate.tpl");
        
    }
    
    function showEditor(){
        
        $title = 'task'; //CUSTOMIZABLE
        $Utitle = 'Task'; //CUSTOMIZABLE
        $classname = 'Task'; //CUSTOMIZABLE
        $main_project_id = $_GET['project_id'];
        //var_dump($main_project_id);
        
        
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."_editor");
        $smarty->assign('Utitle', $Utitle);
        $smarty->assign('main_project_id', $main_project_id);
        
             
        if(empty($_GET['id'])){
            $class = new $classname();
            $project = new Project;
            $projects = $project->getNames();
            $smarty->assign('page', 'new');
            $smarty->assign('projects', $projects);
            
        } else {
            $id = $_GET['id'];
            $class = new $classname($id);
            
            
            if(empty($_POST)){
                $data = $class->getObjectData();
            } else {
                $data = $_POST;
            }
            
            //CUSTOMIZABLES
            $smarty->assign('id', $id);
            $smarty->assign('page', 'edit');
            $smarty->assign('project_id', $data['project_id']);
            $smarty->assign('name', $data['name']);
            $smarty->assign('priority', $data['priority']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
        }
        
               
        if ($_GET['action'] === 'submit'){
            
            if(!empty($_POST)){
                $data = $_POST;
            }
            //CUSTOMIZABLES
            $smarty->assign('project_id', $data['project_id']);
            $smarty->assign('name', $data['name']);
            $smarty->assign('priority', $data['priority']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
            $smarty->assign('id', $id);
            $smarty->assign('project_name', $data['project_name']);
        }
        
        if ($_GET['action'] === 'update'){
            
            //CUSTOMIZABLES
            $smarty->assign('project_id', $data['project_id']);
            $smarty->assign('name', $data['name']);
            $smarty->assign('priority', $data['priority']);
            $smarty->assign('status', $data['status']);
            $smarty->assign('deadline', $data['deadline']);
            $smarty->assign('description', $data['description']);
            $smarty->assign('id', $id);
        }
        
       /******** Validalas ********/
    if(!empty($_GET['action'])){
            
        if (!Validation::validDescription($_POST['name'])){
                
            $warning[] = 'Invalid name!';
        
        }
        
        if (!Validation::validStatus($_POST['status'])){
                
            $warning[] = 'Invalid status!';
        
        }
        
        if (!Validation::validStatus($_POST['status'])){
                
            $warning[] = 'Invalid customer!';
        
        }
        
        //if (!Validation::validDescription($_POST['description'])){
        //        
        //    $warning[] = 'Invalid description!';
        //
        //}
        
        if(empty($warning) && $_GET['action'] === 'submit' && !$class->isUnique('name', $_POST['name'])){
            
            $warning[] = 'Name already exists!';
            
        }
        
        if($_GET['action']==='update' && $_POST['name'] != $data['name']  && !$class->isUnique('name', $_POST['name'])){
       
            $warning[] = 'Name already exists!';
            
        }
        
    }
        
        
        // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
                if (!empty($main_project_id)){
                    $_POST['project_id'] = $main_project_id;
                } else {
                    $_POST['project_id'] = $project->getIdByName($_POST['project_name']);
                }
                
                $_POST['created_by'] = $_SESSION['login_user'];
                if($class->create($_POST)){
                    
                    $smarty->assign('confirm' , $Utitle." added successfully!");
                    
                }
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                $_POST['updated_by'] = $_SESSION['login_user'];
				$_POST['last_update'] = date('Y-m-d G:i:s');
                //var_dump($this->createCurrentTimestamp());
                
                if ($_POST['status'] === 'Completed'){
                    
                    $_POST['complete_date'] = $this->createCurrentTimestamp();
                     
                }
                
                if ($_POST['status'] !== 'Completed'){
                    
                    $_POST['complete_date'] = "0000-00-00 00:00:00";
                     
                }
                    
                if($class->set($_POST)){
                   
                   $smarty->assign('confirm', $Utitle." updated successfully!");
                   
                }
                    
            
                
                
            }
        
        $smarty->display($title."EditorTemplate.tpl");
        
    }
    
    function showEmployeesOnTask(){
        
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'project'; // CUSTOMIZABLE
        $headers = array('ID', 'Name'); // CUSTOMIZABLE
		$class = new $classname();
        $smarty = $this->smarty;
        
        if ($_GET['action'] === 'delete'){
            
			$employee_id = $class->getIdByName($_GET['employee']);
			$task_id = $_GET['id'];
			//var_dump($employee_id);
			//var_dump($project_id);
			$class->deleteLinkTask($employee_id, $task_id);
        }
        //var_dump($this->getTasksCount('6'));
		$task_id = $_GET['id'];
        
        
        $data = $class->getEmployeesOnTask($task_id, $project_id);
		//var_dump($data);
        
        
        if (!empty($_GET['project_id'])){
			$project_id = $_GET['project_id'];
			$smarty->assign('project_id', $project_id);
		}
        
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
		$smarty->assign('id', $_GET['id']);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('permissions', $this->permissions);
        $smarty->display("employeesOnTaskTemplate.tpl");    
        
    }
    
    
    
    function showEmployeeAddToTask(){
        
        $title = 'employee'; //CUSTOMIZABLE
        $Utitle = 'Employee'; //CUSTOMIZABLE
        $classname = 'Task'; //CUSTOMIZABLE
        $id = $_GET['id'];
		$employee = new Employee;
		$class = new $classname($id);
        
		
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('Utitle', $Utitle);
		$smarty->assign('project_managers', $project_managers);
		
		$task_data = $class->getObjectData();
        $project_id = $task_data['project_id'];
        $task_id = $id;
        //var_dump($project_id);
		$employees = $class->getEmployees($task_id, $project_id);
		$smarty->assign('employees', $employees);
		$smarty->assign('id', $id);
		
		if (!empty($_GET['project_id'])){
			$project_id = $_GET['project_id'];
			$smarty->assign('project_id', $project_id);
		}
		
     /******** Validalas ********/
    
        
        
        // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                	
				
				$employee_id = $employee->getIdByName($_POST['employee']);
				
//              var_dump($employee_id);
//				var_dump($task_id);
                if($employee-> createLinkTask($employee_id, $task_id)){
				
					$employees = $class->getEmployees($task_id, $project_id);
					$smarty->assign('employees', $employees);
					$smarty->assign('confirm' , $Utitle." added successfully!");
				}
                
            } 
                    
                
                
            
        
        $smarty->display("employeeAddToTaskTemplate.tpl");
		
	
        
    }
    
}
