<?php

class Utility {
    
    private $menu;
    private $table;
    private $smarty;
    
    function __construct(){
        
        
        $this->table = new Table('utility');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
    
    }
    
    static function showMenubar(){
        
        $smarty = new Smarty;
        $smarty->setTemplateDir('modules/smarty/templates')
               ->setCompileDir('modules/smarty/templates_c');
        $user = new User;
        $user_level = $user->getLevelByUser($_SESSION['login_user']);       
        $permissions = $user->getPermissions();
        
        $smarty->assign("user_level", $user_level);
        $smarty->assign("permissions", $permissions);
        $smarty->display("menuTemplate.tpl");
        
        
    }
    
    
    function existsMenu($menu){
        $data = $this->table->getRowByColumnValue('get_menu', $menu);
        
        if(!empty($data)){
            return true;
        }
        return false;
    }
    
    function getPermissions(){
        
        $username = $_SESSION['login_user'];
        $user = new User;
//        var_dump($user);
        $user_id = $user->getIdByUsername($username);
//        var_dump($user_id);
//        $user_id = '1';
        
        $select = array('*');
        $from = array ('roles');
        $where = array("id IN (SELECT role_id from user_roles WHERE user_id = '".$user_id."')");

        
        $data = $this->table->selectBuilder($select,$from,$where);
        $permissions = $data[0];
//        echo '<pre>';
//        var_dump($data);
//        echo '</pre>';
        
        foreach ($data as $data2) {
         
            
           foreach($data2 as $key => $value){
               
               if ($key != 'id' && $key != 'name' && $permissions[$key] < $value){
                   
                   $permissions[$key] = $value;
                   
               }
               
           }
                
            
        }
        
//        echo '<pre>';
//        var_dump($permissions);
//        echo '</pre>';
        return $permissions;
        
        
    }
    
    function getPermissionKey($menu){
        
        $data = $this->table->getValueByColumn('permission_key', 'get_menu', $menu);
        return $data['permission_key'];
        
    }
    
    function getClass($menu){
        
        $data = $this->table->getValueByColumn('class', 'get_menu', $menu);
        return $data['class'];
        
    }
    
    function getFunction($menu){
        
        $data = $this->table->getValueByColumn('function', 'get_menu', $menu);
        return $data['function'];
        
    }
    
    function getMenu($menu){
    
        return $this->table->getRowByColumnValue('get_menu', $menu);
        
    }
    
    
    
    function create(array $datas){
       
        if($this->isUnique('get_menu', $datas['get_menu'])){
        
            return $this->table->insert($datas);
        
        } else {
            
            return false;
        
        }
        
    }
    
    function getObjectData($id){
        
        return $this->table->getRow($id);
        
    }
    
    
    function getData(){
        
        $field = 'id';
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
   
    }
    
    function searchData($field,$value,$orderby,$direction,$limit_start,$limit_count){
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);    
    }
    
    function set(array $datas){
        
        $old_data = $this->getObjectData();
        
        if($old_data['get_menu'] != $datas['get_menu']){
        
            if($this->isUnique('get_menu', $datas['get_menu'])){
               
                return $this->table->update($this->id, $datas);
            
            }
        
        } else {
            
            return $this->table->update($this->id, $datas);
        
        }
        
        return false;
    }
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    function isUnique($unique_column, $data){
        
        $field = $unique_column;
        $value = $data;
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        if ($this->table->queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count)){
            return false;
        }
        
        return true;
    }
    
    //static function validEmail($email){
    // 
    //    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    //        
    //      return false;
    //    
    //    }
    //    
    //    return true;
    //       
    //}
    //
    //static function validName($name){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^a-zA-Z' ]/iu",$name) || !preg_match("/[^ ]/",$name)) {
    //      
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validUser($user){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^a-zA-Z._ ]/iu",$user) || !preg_match("/[^ ]/",$user)) {
    //      
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validTitle($name){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^a-zA-Z ]/iu",$name) || !preg_match("/[^ ]/",$name)) {
    //        
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validDescription($description){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^0-9a-zA-Z,.?!(): ]/iu",$description)) {
    //        
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validAddress($address){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^0-9a-zA-Z ]/iu",$address)) {
    //        
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validPhoneNumber($number){
    //    
    //    if(preg_match("/^[0-9]{13}$/", $number)) {
    //      
    //        return true;
    //    
    //    }
    //    
    //    return false;
    //    
    //}
    //
    //static function validCustomers($customer){
    //    //"/[a-zA-Z'-]/",
    //    //"/^[a-zA-Z ]*$/"
    //    if (preg_match("/[^0-9a-zA-Z,.' ]/iu",$customer) || !preg_match("/[^ ]/",$customer)) {
    //        
    //        return false;
    //    
    //    }
    //    
    //    return true;
    //}
    //
    //static function validStatus($status){
    //    if (preg_match("/[^a-zA-Z ]/iu",$status)) {
    //            
    //        return false;
    //    
    //    }
    //    
    //    return true;    
    //        
    //}
    
    
    
}
