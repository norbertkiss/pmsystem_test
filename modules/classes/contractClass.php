<?php

class Contract {
    
    private $id;
    private $table;
    private $smarty;
    private $permissions;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('contract');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    }
    
    
    
    function create(array $datas){
       
            return $this->table->insert($datas);
         
    }
    
	
    function getObjectData(){
		
		return $this->table->getRowByColumnValue('employee_id', $this->id);
        
    }
    
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    
    function showEmployeeContractList(){
    
//      var_dump($this->permissions);
        $classname = 'Contract'; // CUSTOMIZABLE
        $title = 'contract'; // CUSTOMIZABLE
        $headers = array('ID', 'Salary', 'Free Days', 'Start Date', 'Expiration Date'); // CUSTOMIZABLE
        $currency = 'Ft.';
		
        if ($_GET['action'] === 'delete'){
            $id = $_GET['id'];
			$conditions = array ("employee_id = '$id'");
			$this->table->deleteByConditions($conditions);
             
        }
        
		$id = $_GET['id'];
        $smarty = $this->smarty;
        $class = new $classname($id);
		$data = $class->getObjectData();
        
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('id', $id);
        $smarty->assign('currency', $currency);
        $smarty->assign('permissions', $this->permissions);
        $smarty->display("employeeContractListTemplate.tpl");
        
    }
    
    
    function showEditor(){
        
   
        $title = 'contract'; //CUSTOMIZABLE
        $Utitle = 'Contract'; //CUSTOMIZABLE
        $classname = 'contract'; //CUSTOMIZABLE
        $class = new $classname;
		
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', '?menu=contract_editor');
               
        if(empty($_GET['id'])){
            
            #Ha contract listabol addolok új szerződést (amennyiben szükséges)
            
            
        } else {
            
            $id = $_GET['id'];
            
            
                $data = $_POST;
            
                $smarty->assign('salary', $data['salary']);
                $smarty->assign('free_days', $data['free_days']);
                $smarty->assign('start_date', $data['start_date']);
                $smarty->assign('expiration_date', $data['expiration_date']);
                $smarty->assign('id', $id);
                $smarty->assign('page', 'new');
                
            
            
        }
               
             /******* VALIDALAS ********/
            
        if($_GET['action']){    
            
            //if (!Utility::validName($_POST['name'])){
            //        
            //    $warning[] = 'Invalid name!';
            //
            //}
            //
            //
            //if (!Utility::validEmail($_POST['email'])){
            //    
            //    $warning[] = 'Invalid email format!';    
            //    
            //}
            //
            //if(!Utility::validPhoneNumber($_POST['phone'])){
            //    
            //    $warning[] = 'Invalid phone number!';
            //    
            //}
            //
            //if(!Utility::validAddress($_POST['address'])){
            //    
            //    $warning[] = 'Invalid address!';
            //    
            //}
            //
            //if($_GET['action'] === 'submit' && !$class->isUnique('email', $_POST['email'])){
            //    
            //    $warning[] = 'Email already taken!';
            //    
            //}
            //
            //if($_GET['action']==='update' && $_POST['email'] != $data['email']  && !$class->isUnique('email', $_POST['email'])){
            //
            //    $warning[] = 'Email already taken!';
            //    
            //}
            
        }
            
            // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                $_POST['employee_id'] = $id;
                //var_dump($_POST);
                if($class->create($_POST)){
                    
                    $smarty->assign('confirm' , $Utitle." added successfully!");
                    
                } else {
					$smarty->assign('warning' , "Error");
				}
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                //if($class->set($_POST)){
                //    
                //    $smarty->assign('confirm', $Utitle." updated successfully!");
                //    
                //}
                
            }
            
            $smarty->display("employeeContractEditorTemplate.tpl");
        
    }
    
}