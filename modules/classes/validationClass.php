<?php

class Validation {

    static function validEmail($email){
     
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            
          return false;
        
        }
        
        return true;
           
    }
    
    static function validName($name){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^a-zA-Z' ]/iu",$name) || !preg_match("/[^ ]/",$name)) {
          
            return false;
        
        }
        
        return true;
    }
    
    static function validUser($user){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^a-zA-Z._ ]/iu",$user) || !preg_match("/[^ ]/",$user)) {
          
            return false;
        
        }
        
        return true;
    }
    
    static function validTitle($name){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^a-zA-Z ]/iu",$name) || !preg_match("/[^ ]/",$name)) {
            
            return false;
        
        }
        
        return true;
    }
    
    static function validDescription($description){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^0-9a-zA-Z,.?!(): ]/iu",$description)) {
            
            return false;
        
        }
        
        return true;
    }
    
    static function validAddress($address){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^0-9a-zA-Z ]/iu",$address)) {
            
            return false;
        
        }
        
        return true;
    }
    
    static function validPhoneNumber($number){
        
        if(preg_match("/^[0-9]{13}$/", $number)) {
          
            return true;
        
        }
        
        return false;
        
    }
    
    static function validCustomers($customer){
        //"/[a-zA-Z'-]/",
        //"/^[a-zA-Z ]*$/"
        if (preg_match("/[^0-9a-zA-Z,.' ]/iu",$customer) || !preg_match("/[^ ]/",$customer)) {
            
            return false;
        
        }
        
        return true;
    }
    
    static function validStatus($status){
        if (preg_match("/[^a-zA-Z ]/iu",$status)) {
                
            return false;
        
        }
        
        return true;    
            
    }
    
    
    
}