<?php

class Role {
    
    private $id;
    private $table;
    private $smarty;
    private $permissions;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('roles');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    }
    
    function create(array $datas){
       
        return $this->table->insert($datas);
           
    }
    
    function set(array $datas){
        
        return $this->table->update($this->id, $datas);
               
    }    
    
    function getObjectData(){
        
        return $this->table->getRow($this->id);
        
    }
    
    function getData(){
        
        $field = 'id';
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
   
    }
    
    
    function getRoles($id){
        
        $select = array('name');
        $from = array ('roles');
        $where = array("id NOT IN (SELECT role_id from user_roles WHERE user_id = '".$id."')");

        $data = $this->table->selectBuilder($select,$from,$where);
        return $data;
        
    }
    
    function getIdByName($name){
        
        $data = $this->table->getValueByColumn('id', 'name', $name);
        return $data['id'];
    }
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    function showRoleList(){
        
        $classname = 'Role';
        $class = new $classname;
        
        
        if($_GET['action'] === 'delete'){
            
            $id = $_GET['id'];
            $class = new $classname($id);
            $class->delete();
            
        }
        
        $data = $class->getData();
        
        $smarty = $this->smarty;
        $smarty->assign('data', $data);
        $smarty->assign('permissions', $this->permissions);
        $smarty->display("roleListTemplate.tpl");
        
    }
    
    function showRoleEditor(){
        
        $classname = 'Role';
        $class = new $classname;
        $smarty = $this->smarty;
        
        
        
        if($_GET['id']){
            
            $id = $_GET['id'];
            $class = new $classname($id);
            $page = 'edit';
            
            
            if (empty($_POST)){
                
                $data = $class->getObjectData();
                $_POST = $data;
                
            }
            
        } else {
            
            $page = 'new';
            
        }
        
            $smarty->assign('id', $id);
            $smarty->assign('page', $page);
            //USERS
            $smarty->assign('name', $_POST['name']);
            $smarty->assign('show_users', $_POST['show_users']);
            $smarty->assign('add_users', $_POST['add_users']);
            $smarty->assign('edit_users', $_POST['edit_users']);
            $smarty->assign('delete_users', $_POST['delete_users']);
            $smarty->assign('show_user_roles', $_POST['show_user_roles']);
            $smarty->assign('add_user_roles', $_POST['add_user_roles']);
            $smarty->assign('delete_user_roles', $_POST['delete_user_roles']);
            //ROLES
            $smarty->assign('show_roles', $_POST['show_roles']);
            $smarty->assign('edit_roles', $_POST['edit_roles']);
            $smarty->assign('add_roles', $_POST['add_roles']);
            $smarty->assign('delete_roles', $_POST['delete_roles']);
            //EMPLOYEES
            $smarty->assign('show_employees', $_POST['show_employees']);
            $smarty->assign('add_employees', $_POST['add_employees']);
            $smarty->assign('show_employee_contracts', $_POST['show_employee_contracts']);
            $smarty->assign('add_employee_contracts', $_POST['add_employee_contracts']);
            $smarty->assign('delete_employee_contracts', $_POST['delete_employee_contracts']);
            $smarty->assign('show_employee_positions', $_POST['show_employee_positions']);
            $smarty->assign('add_employee_positions', $_POST['add_employee_positions']);
            $smarty->assign('delete_employee_positions', $_POST['delete_employee_positions']);
            $smarty->assign('edit_employees', $_POST['edit_employees']);
            $smarty->assign('delete_employees', $_POST['delete_employees']);
            //POSITIONS
            $smarty->assign('show_positions', $_POST['show_positions']);
            $smarty->assign('add_positions', $_POST['add_positions']);
            $smarty->assign('edit_positions', $_POST['edit_positions']);
            $smarty->assign('delete_positions', $_POST['delete_positions']);
            //PROJECTS
            $smarty->assign('show_projects', $_POST['show_projects']);
            $smarty->assign('show_all_projects', $_POST['show_all_projects']);
            $smarty->assign('add_projects', $_POST['add_projects']);
            $smarty->assign('show_project_employees', $_POST['show_project_employees']);
            $smarty->assign('add_project_employees', $_POST['add_project_employees']);
            $smarty->assign('delete_project_employees', $_POST['delete_project_employees']);
            $smarty->assign('edit_projects', $_POST['edit_projects']);
            $smarty->assign('delete_projects', $_POST['delete_projects']);
            $smarty->assign('show_project_tasks', $_POST['show_project_tasks']);
            //TASKS
            $smarty->assign('show_tasks', $_POST['show_tasks']);
            $smarty->assign('show_all_tasks', $_POST['show_all_tasks']);
            $smarty->assign('add_tasks', $_POST['add_tasks']);
            $smarty->assign('edit_tasks', $_POST['edit_tasks']);
            $smarty->assign('delete_tasks', $_POST['delete_tasks']);
            $smarty->assign('show_task_start', $_POST['show_task_start']);
            $smarty->assign('show_task_stop', $_POST['show_task_stop']);
            $smarty->assign('show_task_employees', $_POST['show_task_employees']);
            $smarty->assign('add_task_employees', $_POST['add_task_employees']);
            $smarty->assign('delete_task_employees', $_POST['delete_task_employees']);
            //PROFILE
            $smarty->assign('edit_profile', $_POST['edit_profile']);
        
        
        if($_GET['action'] === 'submit'){
            
            if($class->create($_POST)){
                
                $smarty->assign('confirm', 'Role has been created successfully.');
                
            } else {
                
                $warning[] = 'Name already taken.';
                
            }
            
        }
        
        if ($_GET['action'] === 'update'){
            
            //var_dump($_POST);
            if($class->set($_POST)){
                
                $smarty->assign('confirm', 'Role has been updated successfully.');
                
            } else {
                
                $warning[] = 'Name already taken.';
                
            }
            
        }
        
        $smarty->assign('warning', $warning);
        $smarty->display("roleEditorTemplate.tpl");
        
    }
    
    
    
    
    
}