<?php

class Employee {
    
    private $id;
    private $table;
    private $smarty;
    private $permissions;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('employee');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    
    }
    
    
    
    function create(array $datas){
       
		//var_dump($datas);
		
        if($this->isUnique('email', $datas['email'])){
        
				
            return $this->table->insert($datas);
        
        } else {
            
            return false;
        
        }
        
    }
	
	function getPositions(){
		
		//$select = array('name', 'position');
		$select = array('position.name as Name');
		$from = array ('employee', 'e_position', 'position');
		$where = array("employee.id = e_position.e_id",
					   "e_position.p_id = position.id",
					   "employee.id = '".$this->id."'");
		
		return $this->table->selectBuilder($select,$from,$where);
		
		
	}
	
	function createContract(array $data){
		
		$table = new Table('contract');
		return $this->table->insert($datas);
		
	}
	
	
	function createLinkPosition($employee_id, $position_name){
        
		$position = new Position;
		$position_id = $position->getIdByName($position_name);
		$table = new Table('e_position');
		
		
		$datas = array ('e_id'=>$employee_id,
						'p_id'=>$position_id['id']);
		
		return $table->insert($datas);
		
    }
	
	function deleteLinkPosition($e_id, $p_id){
        
		$table = new Table('e_position');
		$conditions = array ("e_id = '$e_id'",
							 "p_id = '$p_id'");
		return $table->deleteByConditions($conditions);
		
    }
	
	function createLinkProject($employee_id, $project_id){
        
		
		$table = new Table('e_project');
		$datas = array ('employee_id'=>$employee_id,
						'project_id'=>$project_id);
		
		return $table->insert($datas);
		
    }
	
	function deleteLinkProject($employee_id, $project_id){
        
		$table = new Table('e_project');
		$conditions = array ("employee_id = '$employee_id'",
							 "project_id = '$project_id'");
		//var_dump($conditions);
		return $table->deleteByConditions($conditions);
		
    }
	
	function createLinkTask($employee_id, $task_id){
        
		
		$table = new Table('e_task');
		$datas = array ('employee_id'=>$employee_id,
						'task_id'=>$task_id);
		
		return $table->insert($datas);
		
    }
	
	function deleteLinkTask($employee_id, $task_id){
        
		$table = new Table('e_task');
		$conditions = array ("employee_id = '$employee_id'",
							 "task_id = '$task_id'");
		//var_dump($conditions);
		return $table->deleteByConditions($conditions);
		
    }
	
	function getEmployeesName(){
		
		return $this->table->getColumn('name');
		
	}
    
    function getObjectData(){
        
        return $this->table->getRow($this->id);
        
    }
    
    
    function getData(){
        
        $field = 'id';
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
   
    }
    
    function getIdByEmail($email){
        
        return $this->table->getValueByColumn('id', 'email', $email);
        
    }
	
	function getIdByName($name){
        
		$id = $this->table->getValueByColumn('id', 'name', $name);
        return $id["id"];
        
    }
	
	function getEmployeesOnProject($project_id){
		
		$select = array('employee.name', 'employee.id');
		$from = array ('employee', 'e_project');
		$where = array("employee.id = e_project.employee_id",
                               "e_project.project_id = $project_id");
		
		return $this->table->selectBuilder($select,$from,$where);
		
		
	}
	
	function getEmployeesOnTask($task_id){
		
		$select = array('employee.name', 'employee.id');
		$from = array ('employee', 'e_task');
		$where = array("employee.id = e_task.employee_id",
					   "e_task.task_id = $task_id");
		
		return $this->table->selectBuilder($select,$from,$where);
		
		
	}
	
	function getEmployeesByPosition($position){
		
		$select = array('employee.name as Name');
		$from = array ('employee', 'e_position', 'position');
		$where = array("employee.id = e_position.e_id",
					   "e_position.p_id = position.id",
					   "position.name = '".$position."'");
		
		return $this->table->selectBuilder($select,$from,$where);
		
	}
	
	function getEpositionIdByEmployeeId($employee_id){
		
		$table = new Table('e_position');
        
        return $this->table->getValueByColumn('id', 'e_id', $employee_id);
        
    }
    
    function searchData($field,$value,$orderby,$direction,$limit_start,$limit_count){
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);    
    }
    
    function set(array $datas){
        
        $old_data = $this->getObjectData();
        
        if($old_data['email'] != $datas['email']){
        
            if($this->isUnique('email', $datas['email'])){
               
                return $this->table->update($this->id, $datas);
            
            }
        
        } else {
            
            return $this->table->update($this->id, $datas);
        
        }
        
        return false;
    }
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    
    function isUnique($unique_column, $data){
        
        $field = $unique_column;
        $value = $data;
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        if ($this->table->queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count)){
            return false;
        }
        
        return true;
    }
    
    function showList(){
	
//	var_dump($this->permissions);    
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'employee'; // CUSTOMIZABLE
        $headers = array('ID', 'Name', 'Email', 'Address', 'Phone'); // CUSTOMIZABLE
        
        
		if ($_GET['action'] === 'delete'){
            $delete_id = $_GET['id'];
            $field = new $classname($delete_id);
            $field->delete();  
        }
        
        
        $smarty = $this->smarty;
        $class = new $classname();
        $data = $class->getData();
//        var_dump($data);
        $smarty->assign('permissions', $this->permissions);
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->display($title."ListTemplate.tpl");
        
    }
	
	function showEmployeeContractList(){
        
        
        
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'contract'; // CUSTOMIZABLE
        $headers = array('ID', 'Salary', 'Free Days', 'Start Date', 'Expiration Date'); // CUSTOMIZABLE
        
		
        if ($_GET['action'] === 'delete'){
            $e_id = $_GET['id'];
			
             
        }
        
		$id = $_GET['id'];
        $smarty = $this->smarty;
        $class = new $classname($id);
	
      
        //$data = $class->getPositions();
        $smarty->assign('headers', $headers);
        //$smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('id', $id);
        $smarty->assign('$permissions', $this->permissions);
        $smarty->display("employeeContractListTemplate.tpl");
        
    
	}
	
	function showEmployeeContractEditor(){
		
        
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'contract'; // CUSTOMIZABLE
        //$headers = array('ID', 'Salary', 'Free Days', 'Start Date', 'Expration Date'); // CUSTOMIZABLE
        
		
        
        
	$id = $_GET['id'];
        $smarty = $this->smarty;
        $class = new $classname($id);
		
        //$data = $class->getPositions();
        $smarty->assign('headers', $headers);
        //$smarty->assign('data', $data);
        $smarty->assign('title', $title);
        //$smarty->assign('editor', "?menu=add_".$title."");
	$smarty->assign('id', $id);
        $smarty->display("employeeContractEditorTemplate.tpl");
        
    
	}
	
	function showEmployeePositionList(){
        
        
        $classname = 'Employee'; // CUSTOMIZABLE
        $title = 'position'; // CUSTOMIZABLE
        $headers = array('Name'); // CUSTOMIZABLE
        
		
        if ($_GET['action'] === 'delete'){
            $e_id = $_GET['id'];
			
			$position = new Position;
			$p_id = $position->getIdByName($_GET['p_name']);
			
			
            $field = new $classname;
            $field->deleteLinkPosition($e_id, $p_id['id']);  
        }
        
		$id = $_GET['id'];
        $smarty = $this->smarty;
        $class = new $classname($id);
        
        $data = $class->getPositions();
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=add_".$title."");
        $smarty->assign('id', $id);
        $smarty->assign('permissions', $this->permissions);
        $smarty->display("employeePositionListTemplate.tpl");
        
    }
	
	function showAddPositionToEmployee(){
        
        $title = 'employee'; //CUSTOMIZABLE
        $Utitle = 'Employee'; //CUSTOMIZABLE
        $classname = 'Employee'; //CUSTOMIZABLE
		$position = new Position;
		$position_names = $position->getNames();
		$id = $_GET['id'];
        $class = new $classname;
		
		
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
		$smarty->assign('id', $id);
		$smarty->assign('positions', $position_names);
        $smarty->assign('SCRIPT_NAME', '?menu=add_position');
		
		if ($_GET['action'] === 'submit'){
			$smarty->assign('position_name', $_POST['position_name']);
			//var_dump($id);
			if($class->createLinkPosition($id, $_POST['position_name'])){
			
                    
                    $smarty->assign('confirm', "Position added successfully!");
                    
            }
			else {
				$warning['0'] = 'Position already added';
				$smarty->assign('warning', $warning);
			}
		}
		
		//var_dump($_POST);
		
        $smarty->display("employeeAddPositionTemplate.tpl");
    }
    
    function showEditor(){
        
        $title = 'employee'; //CUSTOMIZABLE
        $Utitle = 'Employee'; //CUSTOMIZABLE
        $classname = 'Employee'; //CUSTOMIZABLE
        
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', '?menu=employee_editor');
               
        if(empty($_GET['id'])){
            $smarty->assign('page', 'new');
            $class = new $classname();
            
            $position = new Position;
            $positions = $position->getNames();
            $smarty->assign('positions', $positions); 
            
        } else {
            $id = $_GET['id'];
            $class = new $classname($id);
            
            if(empty($_POST)){
                $data = $class->getObjectData();
            } else {
                $data = $_POST;
            }
            
            $smarty->assign('name', $data['name']);
            $smarty->assign('email', $data['email']);
            $smarty->assign('address', $data['address']);
            $smarty->assign('phone', $data['phone']);
            $smarty->assign('id', $id);
            $smarty->assign('page', 'edit');
        }
               
        if ($_GET['action'] === 'submit'){
            
            if(!empty($_POST)){
                $data = $_POST;
            }
            
            $smarty->assign('name', $_POST['name']);
            $smarty->assign('email', $_POST['email']);
            $smarty->assign('address', $_POST['address']);
            $smarty->assign('phone', $_POST['phone']);
            $smarty->assign('position_name', $_POST['position_name']);
        }
        
        if ($_GET['action'] === 'update'){
            $smarty->assign('name', $_POST['name']);
            $smarty->assign('email', $_POST['email']);
            $smarty->assign('address', $_POST['address']);
            $smarty->assign('phone', $_POST['phone']);
        }
        
             /******* VALIDALAS ********/
            
        if(!empty($_GET['action'])){    
            
            if (!Validation::validName($_POST['name'])){
                    
                $warning[] = 'Invalid name!';
            
            }
            
            
            if (!Validation::validEmail($_POST['email'])){
                
                $warning[] = 'Invalid email format!';    
                
            }
            
            if(!Validation::validPhoneNumber($_POST['phone'])){
                
                $warning[] = 'Invalid phone number!';
                
            }
            
            if(!Validation::validAddress($_POST['address'])){
                
                $warning[] = 'Invalid address!';
                
            }
            
            if($_GET['action'] === 'submit' && !$class->isUnique('email', $_POST['email'])){
                
                $warning[] = 'Email already taken!';
                
            }
            
            if($_GET['action']==='update' && $_POST['email'] != $data['email']  && !$class->isUnique('email', $_POST['email'])){
           
                $warning[] = 'Email already taken!';
                
            }
            
        }
            
            // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
              
                if($class->create($_POST)){
                    
                    $smarty->assign('confirm' , $Utitle." added successfully!");
                    
					$id = $class->getIdByEmail($_POST['email']);
					$class->createLinkPosition($id['id'], $_POST['position_name']);
                }
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                if($class->set($_POST)){
                    
                    $smarty->assign('confirm', $Utitle." updated successfully!");
                    
                }
                
            }
        
        $smarty->display($title."EditorTemplate.tpl");
        
    }
    
}

 
  
  
  
  
  


