<?php

class Login{
    
    private $smarty;
    
    
    function __construct(){
        
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        
        
        
    }
    
    static function startSession(){
        session_start();
    }
    
    static function sessionIsSet(){
        
        if (isset($_SESSION['login_user'])){
            return true;
        }
        
    }
    
    function showLoginForm(){
        
        $smarty = $this->smarty;
        
        if ($_POST['submit']){
            
            if (empty($_POST['username']) || empty($_POST['password'])){
                $error = 'Wrong username or password!';
            } else {
                
                $user = new User;
                $username = $_POST['username'];
                $password = $_POST['password'];
                
                //var_dump($user->validLogin($username, $password));
                
                if($user->validLogin($username, $password)){
                    
                    $_SESSION['login_user'] = $username;
                    echo "<meta http-equiv=\"refresh\" content=\"0;url=\">";
                    
                } else {
                    
                    $error = 'Wrong username or password!';
                    
                }
                
                
                
            }
            
        }
        
        
        
        $smarty->assign('error', $error);            
        $smarty->display("loginFormTemplate.tpl");
        
    }
    
    function showControlPanel(){
        
        $smarty = $this->smarty;
        
        $user = $_SESSION['login_user'];
        
        if ($_POST['submit'] === 'Logout'){
            
            $this->endSession();
            echo "<meta http-equiv=\"refresh\" content=\"0;url=?menu=\">";
            
        }
        
        $role = new User;
        $permissions = $role->getPermissions();
        
//        var_dump($permissions);
        $smarty->assign('user', $user);
        $smarty->assign('permissions', $permissions);
        $smarty->display('controlPanelTemplate.tpl');
        
        
    }
    
    function showProfile(){
        
       
        
        $title = 'profile'; //CUSTOMIZABLE
        $Utitle = 'Profile'; //CUSTOMIZABLE
        $classname = 'User'; //CUSTOMIZABLE
        $class = new $classname;
        $username = $_SESSION['login_user'];
        
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."");
               
               
        $id = $class->getIdByUsername($username);
        $class = new $classname($id);
        $user_data = $class->getObjectData();
        //var_dump($user_data);
        
        if (empty($_POST)){
            
            $smarty->assign('username', $user_data['username']);
            
        } else {
            
            $smarty->assign('username', $_POST['username']);
            $smarty->assign('password', $_POST['password']);
            $smarty->assign('new_password', $_POST['new_password']);
            $smarty->assign('re_password', $_POST['re_password']);
        
        }
            
        
             /******* VALIDALAS ********/
             
        if($_GET['action'] === 'submit'){
        
             
            if($_POST['password'] != $user_data['password']){
                
                $warning[] = 'Wrong password!';
            
            }
            
            if($_POST['new_password'] != $_POST['re_password']){
                
                $warning[] = 'Your new password does not match the confirm password.';
                
            }
            
            
            if($_POST['username'] != $_SESSION['login_user'] && !$class->isUnique('username', $_POST['username'])){
                
                $warning[] = 'Username already taken!';
                
            }
            
            
             
            
        //if(!empty($_GET['action'])){    
        //    
        //    if (!Validation::validName($_POST['name'])){
        //            
        //        $warning[] = 'Invalid name!';
        //    
        //    }
        //    
        //    
        //    if (!Validation::validEmail($_POST['email'])){
        //        
        //        $warning[] = 'Invalid email format!';    
        //        
        //    }
        //    
        //    if(!Validation::validPhoneNumber($_POST['phone'])){
        //        
        //        $warning[] = 'Invalid phone number!';
        //        
        //    }
        //    
        //    if(!Validation::validAddress($_POST['address'])){
        //        
        //        $warning[] = 'Invalid address!';
        //        
        //    }
        //    
        //    if($_GET['action'] === 'submit' && !$class->isUnique('email', $_POST['email'])){
        //        
        //        $warning[] = 'Email already taken!';
        //        
        //    }
        //    
        //    if($_GET['action']==='update' && $_POST['email'] != $data['email']  && !$class->isUnique('email', $_POST['email'])){
        //   
        //        $warning[] = 'Email already taken!';
        //        
        //    }
        //    
        }
            
            // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
                if($_POST['new_password']){
                    
                    $_POST['password'] = $_POST['new_password'];
                    
                }
                
                if($class->set($_POST)){
                    
                    $_SESSION['login_user'] = $_POST['username'];
                    echo "<meta http-equiv=\"refresh\" content=\"0;url=?menu=user\">";
                    
                }
                
                
                //if (empty($_POST['employee_name'])){
                //    
                //    $_POST['employee_id'] = NULL;
                //} else {
                //    
                //    $employee = new Employee;
                //    $_POST['employee_id'] = $employee->getIdByName($_POST['employee_name']);
                //}
                //
                //var_dump($_POST);
                //if($class->create($_POST)){
                //    
                //    $smarty->assign('confirm' , $Utitle." added successfully!");
                //    
                //} else {
                //    
                //    $smarty->assign('warning' , "Error");
                //}
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                //if($class->set($_POST)){
                //    
                //    $smarty->assign('confirm', $Utitle." updated successfully!");
                //    
                //}
                
            }
        
        $smarty->display("profileTemplate.tpl");
        
    
        
    }
    
    
    
    static function endSession(){
        
        session_destroy();
        
    }
    
    
    
}