<?php

class Table {
    
    private $db;
    private $table;
    private $driver;
    
    
    function __construct($table){
        
        $this->db = Mysql_Database::getInstance();
        $this->table = $table;
        $this->driver = DBConfig::$driver.'_Database';
        
    }
                                      
    function getDB(){
        return $this->db;
    }
    
    function selectBuilder(array $select, array $from, array $where){
        
        if (count ($select) > '1'){
            $select = implode(",", $select);
        } else {
            $select = $select['0'];
        }
        
        if (count ($from) > '1'){
            $from = implode(",", $from);
        } else {
            $from = $from['0'];
        }
        
        if (count ($where) > '1'){
            $where = implode(" AND ", $where);
        } else {
            $where = $where['0'];
        }
        
        $sql = "SELECT $select FROM $from WHERE $where";
        //var_dump($sql);
        $res=$this->db->select($sql);
        return $res;                           
    }
    
    function leftJoinBuilder(array $select, array $from, array $where){
        
        if (count ($select) > '1'){
            $select = implode(",", $select);
        } else {
            $select = $select['0'];
        }
        
        if (count ($from) > '1'){
            $from = implode(",", $from);
        } else {
            $from = $from['0'];
        }
        
        if (count ($where) > '1'){
            $where = implode(" AND ", $where);
        } else {
            $where = $where['0'];
        }
        
        $sql = "SELECT $select FROM $from ON $where";
        //var_dump($sql);
        $res=$this->db->select($sql);
        return $res;                           
    }
    
    function queryObjects($field,$value,$orderby,$direction,$limit_start,$limit_count){
        
        $sql="SELECT * FROM ".$this->table." WHERE $field LIKE '%$value%' ORDER BY $orderby $direction LIMIT $limit_start,$limit_count" ;
        $res=$this->db->select($sql);
        return $res;
    
    }
    
    function queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count){
        
        $sql="SELECT * FROM ".$this->table." WHERE $field LIKE '$value' ORDER BY $orderby $direction LIMIT $limit_start,$limit_count" ;
        $res=$this->db->select($sql);
        return $res;
    
    }
    
    function queryJoinedObjects(array $select, $tabla_ketto,$kulcs_egy,$kulcs_ketto,$oszlop,$ertek=NULL){
        
        $select = implode(",", $select);
        
        $sql="SELECT $select FROM ".$this->table." INNER JOIN $tabla_ketto WHERE ".$this->table.".".$kulcs_egy." = ".$tabla_ketto.".".$kulcs_ketto." AND $oszlop LIKE '%$ertek%'";
        $res=$this->db->select($sql);
        return $res; 
    }
    
    function queryJoinedUniqueObjects(array $select, $tabla_ketto,$kulcs_egy,$kulcs_ketto,$oszlop,$ertek){
        
        $select = implode(",", $select);
        
        $sql="SELECT $select FROM ".$this->table." INNER JOIN $tabla_ketto WHERE ".$this->table.".".$kulcs_egy." = ".$tabla_ketto.".".$kulcs_ketto." AND $oszlop LIKE '$ertek'";
        $res=$this->db->select($sql);
        return $res;
    
    }
     
    function insert(array $data){
    //FULLY PROTECTED FROM SQL INJECTION    
        foreach($data as $key=>$value){
            
            $validation = true;
            
            if($this->db->columnExists($key, $this->table) === true){
                
                
                if ($this->validateType($key, $value) === false){
                    
                    $validation = false;
                    
                }
                
            }
            
            if ($this->db->columnExists($key, $this->table) === true){
                $names[] = $key;
                $values[] = $value;
                $unnamed_placeholders[] = '?';
            }
            
        }
        //var_dump($validation);
        //return;
        if ($validation === true){
            
            $names = implode(",", $names);
            $names = "(".$names.")";
            
            $placeholders = implode(",", $unnamed_placeholders);
            $placeholders = "(".$placeholders.")";
            
            $sql = "INSERT INTO ".$this->table." ".$names." VALUES ".$placeholders."";
//            var_dump($sql);
            $ret = $this->db->query($sql, $values);
            
        } else {
            
            return false;
            
        }
        
        return $ret;
        
    }
    
    function update($id, array $data){
    //FULLY PROTECTED FROM SQL INJECTION
        $validation = true;
        $ret = true;
        
        foreach($data as $key=>$value){
            
            if($this->db->columnExists($key, $this->table) === true){
            
                if ($this->validateType($key, $value) === false){
                    
                    $validation = false;
                    
                }
                
            }
        
        }
        
        if($validation == true){
        
            foreach($data as $key=>$value){
            
                if($this->db->columnExists($key, $this->table) === true && $ret == true ){
                        $values[0]=$value;    
                        $sql = "UPDATE ".$this->table." SET $key = ? WHERE id = '$id'"; 
                        $ret = $this->db->query($sql,$values);
                            
                } else {
                    echo 'Update error';
                }
                
            }
            return $ret;
        
        } else {
            
            echo 'Validation error';
            
        }
    }
    
    
    
    
    function delete($id){
        
        $sql = "DELETE FROM ".$this->table." WHERE id = '$id'";
        $this->db->query($sql);
        
    }
    
    function deleteByConditions(array $conditions){
        
        if (count ($conditions) > '1'){
            $conditions = implode(" AND ", $conditions);
        } else {
            $conditions = $conditions['0'];
        }
        
        $sql = "DELETE FROM ".$this->table." WHERE $conditions";
        //var_dump($sql);
        $this->db->query($sql);
        
    }
    
    function getRow($id){
        
        $res = $this->db->select("SELECT * FROM ".$this->table." WHERE id = '$id'");
        return $res[0];
        
    }
    
    function getRowByColumnValue($column, $value){
        
        $res = $this->db->select("SELECT * FROM ".$this->table." WHERE $column = '$value'");
        return $res;
        
    }
    
    function getColumn($column){
        $sql = "SELECT $column FROM ".$this->table."";
        //var_dump($sql);
        return $this->db->select($sql);
        
    }
    
    function getValueByColumn($column1, $column2, $value){
        $sql = "SELECT $column1 FROM ".$this->table." WHERE $column2 = '$value'";
        //var_dump($sql);
        $res = $this->db->select($sql);
        return $res[0];
        
    }
    
    function getColumnType($column){
       
        $Column = $this->db->query("SELECT $column FROM ".$this->table."");
        $meta = $Column->getColumnMeta(0);
        $types = Mysql_Database::$datatypes;
        
        foreach($types as $key => $value){
            
            if ($meta['native_type'] == $key){
            
                return $value;
                
            } 
            
        }
            
    }
    
    function isUnique($column, $data){
        
        $sql = "SELECT * FROM ".$this->table." WHERE $column = '$data'";
        
        $data = $this->db->select($sql);
        
        
        if (empty ($data)){
            
            return true;
        }
        
        return false;
        
    }
    
    
    
    function validateType($column, $data){
        
        if ($this->getColumnType($column) === 'int' && (is_numeric($data) || $data===NULL)){
            
            return true;
        
        }
        
        if ($this->getColumnType($column) === 'string' && is_string($data)){    
            
            return true;
        
        }
        
        if ($this->getColumnType($column) === 'date' && !preg_match("^\d{4}[.-/]\d{1,2}[.-/]\d{2}^", $data)){    
            
            return true;
        
        }
        
         if ($this->getColumnType($column) === 'timestamp' && !preg_match("^\d{4}[.-/]\d{1,2}[.-/]\d{2}[\\S]\d{2}[:]\d{2}[:]\d{2}^", $data)){    
            
            return true;
        
        }
        
        var_dump($this->getColumnType($column));
        var_dump($column);
        //var_dump($data);
        
        return false;
        
    }
    
    function getLastInsertID(){
		return $this->db->getLastInsertID();  	   
	}
    
    
    
       
}