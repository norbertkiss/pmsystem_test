<?php

class Position {
    
    private $id;
    private $table;
    private $smarty;
    private $permissions;
    
    function __construct($id){
        
        $this->id = $id;
        $this->table = new Table('position');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $utility = new Utility;
        $this->permissions = $utility->getPermissions();
    }
    
    function create(array $datas){
       
        if($this->isUnique('name', $datas['name'])){
        
            return $this->table->insert($datas);
        
        }
        
        return false;
    }
    
    function getIdByName($name){
        
        return $this->table->getValueByColumn('id', 'name', $name);
        
    }
    
  
    function getObjectData(){
        return $this->table->getRow($this->id);
        
    }
    
    function getNames(){
        
        $table = new Table('position');
        return $table->getColumn('name');
        
    }
    
    
    function getData(){
        $field = 'id';
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);
    }
    
    function searchData($field,$value,$orderby,$direction,$limit_start,$limit_count){
        return $this->table->queryObjects($field,$value,$orderby,$order_dir,$limit_start,$limit_count);    
    }
    
    function set(array $datas){
        
        return $this->table->update($this->id, $datas);
            
    } 
        
       
    
    function delete(){
        return $this->table->delete($this->id);
    }
    
    function isUnique($unique_column, $data){
        $field = $unique_column;
        $value = $data;
        $orderby = 'id';
        $order_dir = 'ASC';
        $limit_start = '0';
        $limit_count = '9999';
        
        if ($this->table->queryUniqueObjects($field,$value,$orderby,$direction,$limit_start,$limit_count)){
            return false;
        }
        
        return true;
    }
    
    function showList(){
        
        $classname = 'Position'; // CUSTOMIZABLE
        $title = 'position'; // CUSTOMIZABLE
        $headers = array('ID', 'Name'); // CUSTOMIZABLE
        
        if ($_GET['action'] === 'delete'){
            $delete_id = $_GET['id'];
            $field = new $classname($delete_id);
            $field->delete();  
        }
        
        $smarty = $this->smarty;
        $class = new $classname();
        $data = $class->getData();
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('permissions', $this->permissions);
        $smarty->display($title."ListTemplate.tpl");
        
    }
    
    function showEditor(){
        
        $title = 'position'; //CUSTOMIZABLE
        $Utitle = 'Position'; //CUSTOMIZABLE
        $classname = 'Position'; //CUSTOMIZABLE
        
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."_editor");
        $smarty->assign('Utitle', $Utitle);
             
        if(empty($_GET['id'])){
            $smarty->assign('page', 'new');
            $class = new $classname();
            
         
        } else {
            $id = $_GET['id'];
            $class = new $classname($id);
            
            if(empty($_POST)){
                $data = $class->getObjectData();
            } else {
                $data = $_POST;
            }
            
            //CUSTOMIZABLES
            $smarty->assign('name', $data['name']);
            
            $smarty->assign('id', $id);
            $smarty->assign('page', 'edit');
        }
        
               
        if ($_GET['action'] === 'submit'){
            if(!empty($_POST)){
                $data = $_POST;
            }
            
            //CUSTOMIZABLES
            $smarty->assign('name', $data['name']);
            $smarty->assign('rank', $data['rank']);
            $smarty->assign('id', $id);
        }
        
        if ($_GET['action'] === 'update'){
            
            //CUSTOMIZABLES
            $smarty->assign('name', $data['name']);
            
            $smarty->assign('id', $id);
        }
        
                /******* VALIDALAS ********/
        if(!empty($_GET['action'])){
        
            if (!Validation::validName($_POST['name'])){
                    
                $warning[] = 'Invalid name!';
            
            }
            
            if(empty($warning) && $_GET['action'] === 'submit' && !$class->isUnique('name', $_POST['name'])){
                
                $warning[] = 'Name already exists!';
                
            }
            
            if($_GET['action']==='update' && $_POST['name'] != $data['name']  && !$class->isUnique('name', $_POST['name'])){
           
                $warning[] = 'Name already exists!';
                
            }
            
        }
            
           
            // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
                if($class->create($_POST)){
                    
                    $smarty->assign('confirm' , $Utitle." added successfully!");
                    
                }  
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                 if($class->set($_POST)){
                    
                    $smarty->assign('confirm', $Utitle." updated successfully!");
                    
                } else {
                    
                    var_dump($class->set($_POST));
                }
                
            }
        
        $smarty->display($title."EditorTemplate.tpl");
        
    }
    
}