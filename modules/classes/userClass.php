<?php

class User {
    
    private $id;
    private $table;
    private $smarty;
    private $permissions;
    
    function __construct($id){
        
		
        $this->id = $id;
        $this->table = new Table('user');
        $this->smarty = new Smarty;
        $this->smarty->setTemplateDir('modules/smarty/templates')
                     ->setCompileDir('modules/smarty/templates_c');
        
        $this->permissions = $this->getPermissions();
        
    }
    
    
    
    function create(array $datas){
       
            return $this->table->insert($datas);
         
    }
    
    function getPermissions(){
        
        $username = $_SESSION['login_user'];
        
//      
        $user_id = $this->getIdByUsername($username);
//        var_dump($user_id);
//        $user_id = '1';
        
        $select = array('*');
        $from = array ('roles');
        $where = array("id IN (SELECT role_id from user_roles WHERE user_id = '".$user_id."')");

        
        $data = $this->table->selectBuilder($select,$from,$where);
        $permissions = $data[0];
//        echo '<pre>';
//        var_dump($data);
//        echo '</pre>';
        
        foreach ($data as $data2) {
         
            
           foreach($data2 as $key => $value){
               
               if ($key != 'id' && $key != 'name' && $permissions[$key] < $value){
                   
                   $permissions[$key] = $value;
                   
               }
               
           }
                
            
        }
        
//        echo '<pre>';
//        var_dump($permissions);
//        echo '</pre>';
        return $permissions;
        
        
    }
	
    function getIdByUsername($username){

        $table = new Table('user');
        $id = $table->getValueByColumn('id', 'username', $username);
        return $id['id'];

    }
    
    function getEmployeeIdByUserId($user_id){

        $table = new Table('user');
        $id = $table->getValueByColumn('employee_id', 'id', $user_id);
        return $id['employee_id'];

    }
    
    
	
	function getLevelByUser($username){
		
		$table = new Table('user');
        $data = $table->getValueByColumn('level', 'username', $username);
        return $data['level'];
		
	}
	
	
	
    
    function getObjectData(){
        
        return $this->table->getRow($this->id);
        
    }
    
    
    function getData(){
        
        $select = array('user.id', 'username', 'password', 'level', 'employee.name');
		$from = array ('user LEFT JOIN employee');
		$where = array("user.employee_id = employee.id");
        
        
        return $this->table->leftJoinBuilder($select,$from,$where);
   
    }
    
    function getEmployees(){
        
        $select = array('name');
		$from = array ('employee');
		$where = array("id NOT IN (SELECT employee_id from user WHERE employee_id != 'NULL')");
        
        $data = $this->table->selectBuilder($select,$from,$where);
		return $data;
        
    }
    
    function getRolesByUser($user_id){
		
		$select = array('roles.id', 'roles.name');
		$from = array ('user_roles', 'roles');
		$where = array("roles.id = user_roles.role_id",
                               "user_roles.user_id = '$user_id'");
		
		return $this->table->selectBuilder($select,$from,$where);
		
		
    }
    
    
    function set(array $datas){
                   
       return $this->table->update($this->id, $datas);
            
    }
    
    
    function delete(){
        return $this->table->delete($this->id);
    }
	
    function validLogin($name,$password){

            $pass = $this->table->getValueByColumn('password', 'username', $name);

            if ($pass['password'] === $password){
                    return true;
            }

    }
	
    function isUnique($column, $data){

            return $this->table->isUnique($column, $data);

    }
    
    function createUserRoleLink($user_id, $role_id){
        
        $table = new Table('user_roles');
        $datas = array ('user_id'=>$user_id,
                        'role_id'=>$role_id);

        return $table->insert($datas);
        
    }
    
    function deleteUserRoleLink($user_id, $role_id){
        
        $table = new Table('user_roles');
        $conditions = array ("user_id = '$user_id'",
                             "role_id = '$role_id'");
        //var_dump($conditions);
        return $table->deleteByConditions($conditions);
        
    }
    
    
	
    
    function showList(){
	
        
        $classname = 'User'; // CUSTOMIZABLE
        $title = 'user'; // CUSTOMIZABLE
        $headers = array('ID', 'Employee', 'Username', 'Password'); // CUSTOMIZABLE
        
        
		if ($_GET['action'] === 'delete'){
            $delete_id = $_GET['id'];
            $field = new $classname($delete_id);
            $field->delete();  
        }
        
        $smarty = $this->smarty;
        $class = new $classname();
        $data = $class->getData();
        $smarty->assign('headers', $headers);
        $smarty->assign('data', $data);
        $smarty->assign('title', $title);
        $smarty->assign('editor', "?menu=".$title."_editor");
        $smarty->assign('permissions', $this->permissions);
        $smarty->display($title."ListTemplate.tpl");
        
    }
	
    
    function showEditor(){
        
        $title = 'user'; //CUSTOMIZABLE
        $Utitle = 'User'; //CUSTOMIZABLE
        $classname = 'User'; //CUSTOMIZABLE
        
        $smarty = $this->smarty;
        $smarty->assign('title', $title);
        $smarty->assign('SCRIPT_NAME', "?menu=".$title."_editor");
               
        if(empty($_GET['id'])){
            $smarty->assign('page', 'new');
            $class = new $classname();
            $employees = $class->getEmployees();
            $smarty->assign('employees', $employees);
            
            
            if(!empty($_POST)){
                
                $data = $_POST;
                $smarty->assign('username', $data['username']);
                $smarty->assign('password', $data['password']);
                $smarty->assign('level', $data['level']);
                
                
                if($_POST['employee_name']){ 
                    $smarty->assign('employee_name', $data['employee_name']);
                }
            }
            
            
            
        } else {
            $id = $_GET['id'];
            $class = new $classname($id);
			$data = $class->getObjectData();
            $smarty->assign('id', $id);
            $smarty->assign('page', 'edit');
            
            if(empty($_POST)){
                
                $smarty->assign('username', $data['username']);
                $smarty->assign('password', $data['password']);
                $smarty->assign('level', $data['level']);
                
            } else {
                
                if(!empty($_POST)){
                
                $smarty->assign('username', $_POST['username']);
                $smarty->assign('password', $_POST['password']);
                $smarty->assign('level', $_POST['level']);
                
        }
                
            }
            
        }
        
             /******* VALIDALAS ********/
			 
		if(!empty($_GET['action'])){
			 
			if(empty($_GET['id']) && !$class->isUnique('username', $_POST['username'])){
					
					$warning[] = 'Username already taken!';
					
			} elseif ($data['username'] != $_POST['username'] && !$class->isUnique('username', $_POST['username'])){
				
				$warning[] = 'Username already taken!';
				
			}
			
			if (!Validation::validUser($_POST['username'])){
						
					$warning[] = 'Invalid name!';
				
			}
		
        }
		
		//var_dump(Validation::validUser($_POST['username']));
		//return;
            
            // HIBA KIIRATAS
            if (!empty($warning)){
                
                $smarty->assign('warning', $warning);
                
            //Uj sor hozzaadasa a tablahoz    
            } elseif($_GET['action'] === 'submit') {
                
                if (empty($_POST['employee_name'])){
                    $_POST['employee_id'] = NULL;
					
                } else {
                    
                    $employee = new Employee;
                    $_POST['employee_id'] = $employee->getIdByName($_POST['employee_name']);
                }
				
				
				
                if($class->create($_POST)){
                    
                    echo "<meta http-equiv=\"refresh\" content=\"0;url=?menu=user\">";
                    
                } else {
                    
                    $smarty->assign('warning' , "Error");
                }
            
            //Letezo sor modositasa
            } elseif($_GET['action']==='update'){
                
                if($class->set($_POST)){
                    
                    $smarty->assign('confirm', $Utitle." updated successfully!");
                    
                }
                
            }
        
        $smarty->display($title."EditorTemplate.tpl");
        
    }
    
    function showUserRolesList(){
        
        
        $classname = 'User'; // CUSTOMIZABLE
        $id = $_GET['id'];
        
	if ($_GET['action'] === 'delete'){
            $role_id = $_GET['delete_id'];
            $class = new $classname();
            
            $class->deleteUserRoleLink($id, $role_id);  
        }
        
        
        $class = new $classname();
        $data = $class->getRolesByUser($id);
        
        $smarty = $this->smarty;
        $smarty->assign('data', $data);
        $smarty->assign('id', $id);
        $smarty->assign('permissions', $this->permissions);
        $smarty->display('userRolesListTemplate.tpl');
        
    
    }
    
    function showUserRolesEditor(){
        
        $smarty = $this->smarty;
        $classname = 'User'; //CUSTOMIZABLE
        $id = $_GET['id'];
        
        $role = new Role;
        $class = new $classname;
        $roles = $role->getRoles($id);
		
        $smarty = $this->smarty;
        $smarty->assign('roles', $roles);
        $smarty->assign('id', $id);
        
        
		
        if ($_GET['action'] === 'submit'){
            
            $role_id = $role->getIdByName($_POST['role']);
            
            if($class->createUserRoleLink($id, $role_id)){
                
                $smarty->assign('confirm', "Role added successfully!");
                $roles = $role->getRoles($id);
                $smarty->assign('roles', $roles);
                
            }
                
        }

        //var_dump($_POST);
		
        $smarty->display('userRolesEditorTemplate.tpl');
    
        
    }
    
    
    
}