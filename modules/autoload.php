<?php

class Loader{
  private $classes;
  
  function __construct(array $classes=null){
    $this->classes=$classes;    
  }
  
  function load($class){
    require __DIR__.'/'.$this->classes[$class];
  }
}

$loader=new Loader(include __DIR__.'/config/'."loaderconfig.php");

spl_autoload_register(array($loader,'load'));