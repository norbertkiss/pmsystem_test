<?php

return array(
   'DBConfig'         =>'config/dbconfig.php'
  ,'Database'         =>'classes/databaseCoreClass.php'
  ,'Mysql_Database'   =>'classes/databaseMysqlClass.php'
  ,'Table'            =>'classes/databaseTableClass.php'
  ,'Utility'          =>'classes/utilityClass.php'
  ,'Employee'         =>'classes/employeeClass.php'
  ,'Position'         =>'classes/positionClass.php'
  ,'Project'          =>'classes/projectClass.php'
  ,'Task'             =>'classes/taskClass.php'
  ,'Contract'         =>'classes/contractClass.php'
  ,'User'             =>'classes/userClass.php'
  ,'Role'             =>'classes/roleClass.php'
  ,'Login'            =>'classes/loginClass.php'
  ,'FluentPDO'        =>'querybuilder/FluentPDO/FluentPDO.php'
  ,'Validation'       =>'classes/validationClass.php'
  
  
);